#include <iostream>
//64비트로 해야함
using namespace std;
extern "C"
{
#include "luaLib/include/lauxlib.h"
#include "luaLib/include/lua.h"
#include "luaLib/include/lua.hpp"
#include "luaLib/include/luaconf.h"
#include "luaLib/include/lualib.h"
}

#pragma comment(lib,"luaLib/lua54.lib")

int C_plusAB(lua_State* LS)
{
	int a = (int)lua_tonumber(LS,-2);
	int b = (int)lua_tonumber(LS, -1);
	lua_pop(LS, 3);// 함수도 필요없으니깐 3개
	lua_pushnumber(LS, a + b);
	return 1; //성공했다고 1리턴함
}


int main()
{

	
	lua_State* LS = luaL_newstate();// 루아 오픈/가상머신 생성

	luaL_openlibs(LS);//표준라이브러리 오픈
	luaL_loadfile(LS, "Test.lua");
	
	int err=lua_pcall(LS, 0, 0, 0); //실행주체, 파라미터, 리턴값, 에러처리
	if (err != 0) {
		cout << "Error: " << lua_tostring(LS, -1);
		lua_pop(LS, 1);
	}

	lua_register(LS, "C_plusAB", C_plusAB);
	lua_getglobal(LS, "plusAB");
	lua_pcall(LS, 0, 1, 0);

	int ret = (int)lua_tonumber(LS, -1);
	lua_pop(LS, 1);
	cout << ret << endl;
	lua_close(LS);





}