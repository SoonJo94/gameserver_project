#include <iostream>
#include <thread>
#include <mutex>
#include "CDeadLockFinder.h"
using namespace std;
constexpr int CNT = 2000;



CLock Alock("ALock");
CLock Block("BLock");

volatile int A = 0;
volatile int B = 0;


void ThreadA()
{
	for (int i = 0; i < CNT; ++i)
	{

		Alock.Lock();

		A += 2;

		Block.Lock();

		B += 2;

		Block.Unlock();


		Alock.Unlock();

	}

}

void ThreadB()
{

	for (int i = 0; i < CNT; ++i)
	{


		Block.Lock();

		B += 2;


		Alock.Lock();

		A += 2;


		Alock.Unlock();


		Block.Unlock();

	}
}

int main()
{
	thread tA(ThreadA);
	thread tB(ThreadB);

#ifdef __DEBUG
	g_deadLockFinder;
#endif // __DEBUG


	tA.join();
	cout << "threadA  ���� A: " << A << '\n';
	tB.join();
	cout << "threadB  ���� B: " << B << '\n';
}