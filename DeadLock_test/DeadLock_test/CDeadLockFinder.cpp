#include "CDeadLockFinder.h"
#include <cassert>



#ifdef _DEBUG
thread_local std::stack<int> tls_lockStack;

void CDeadlockFinder::PushLock(const std::string& lockID)
{
	std::lock_guard<std::mutex> gl(m_lock); //lock
	int simpleID = -1;
	auto iter = m_lockTable.find(lockID);
	if (iter == m_lockTable.end()) {//새로운 lock이다.
		simpleID = m_lockTable.size();
		m_lockTable[lockID] = simpleID;
		m_lockTableReverse[simpleID] = lockID;
	}
	else { //이미 존재하는 lock이다.
		simpleID = iter->second;
	}
	if (tls_lockStack.empty() == false) { //락스택에 다른락있으면 데드락 검사해야함
		const int existedId = tls_lockStack.top();
		if (existedId != simpleID) {
			auto& lockList = m_lockList[existedId];
			if (lockList.find(simpleID) == lockList.end()) { //이전에 참조안된 새로운 락일경우에만 검사
				lockList.insert(simpleID);
				CheckDeadLock();
			}
		}
	}

	tls_lockStack.push(simpleID);

}

void CDeadlockFinder::PopLock(const std::string& lockID)
{
	std::lock_guard<std::mutex> gl(m_lock); //lock
	tls_lockStack.pop();
}

void CDeadlockFinder::CheckDeadLock()
{
	const int lockCnt = m_lockTable.size();
	m_discover = std::vector<int>(lockCnt, -1);
	m_parents = std::vector<int>(lockCnt, -1);
	m_finished = std::vector<bool>(lockCnt, false);
	m_discoverCnt = 0;
	for (int i = 0; i < lockCnt; ++i)
		DFS(i);

	m_discover.clear();
	m_parents.clear();
	m_finished.clear();
}

void CDeadlockFinder::DFS(int cur)
{
	if (m_discover[cur] != -1) //이미 방문했음
		return;

	m_discover[cur] = m_discoverCnt++;
	auto it = m_lockList.find(cur);
	if (it == m_lockList.end()) {//자신이 마지막임
		m_finished[cur] = false;
		return;
	}
	auto& nextList = it->second;
	for (int next : nextList) {

		if (m_discover[next] == -1) { //아직 방문한적없음
			m_parents[next] = cur; //방문할 노드의 부모는 현재
			DFS(next);
			continue;
		}
		if (m_discover[cur] < m_discover[next])//순방향
			continue;
		if (m_finished[next] == false) //아직 역방향의 것이 안끝난 상황
		{
			printf("DeadLock!\n");
			printf("%s -> %s\n", m_lockTableReverse[cur].c_str(), m_lockTableReverse[next].c_str());
			int now = cur;
			while (true) {
				printf("%s -> %s\n", m_lockTableReverse[m_parents[now]].c_str(), m_lockTableReverse[now].c_str());
				now = m_parents[now];
				if (now == next)
					break;
			}

			assert(false,"deadLock");
		}
	}
	m_finished[cur] = true;//끝남

}

CDeadlockFinder g_deadLockFinder;
#endif // _DEBUG

void CLock::Lock()
{
	static int ID = 0;
#ifdef _DEBUG
	g_deadLockFinder.PushLock(m_name);
#endif // _DEBUG

	m_lock.lock();
}

void CLock::Unlock()
{
#ifdef _DEBUG
	g_deadLockFinder.PopLock(m_name);
#endif // _DEBUG
	m_lock.unlock();
}
