#pragma once
#include <unordered_map>
#include <set>
#include <mutex> //이건 본프로젝트에서 빼야함
#include <stack>

#ifdef _DEBUG
extern thread_local std::stack<int> tls_lockStack;
class CDeadlockFinder
{
private:
	std::unordered_map <std::string, int> m_lockTable;//lockID를 받아서 쉬운 ID로 변환(lockTable의 수만큼)
	std::unordered_map < int, std::string> m_lockTableReverse;//쉬운ID를 lockID로
	std::unordered_map <int, std::set<int>> m_lockList;//lockID에 포함된 list

	std::mutex m_lock;

	void CheckDeadLock();
	void DFS(int cur);

private:
	std::vector<int> m_discover;//사이클을 확인하기위해
	std::vector<int> m_parents;
	int m_discoverCnt;
	std::vector<bool> m_finished; //탐색끝
public:
	void PushLock(const std::string& lockID);
	void PopLock(const std::string& lockID);

};
extern CDeadlockFinder g_deadLockFinder;
#endif // _DEBUG
class CLock
{
private:
	std::mutex m_lock;
	std::string m_name;
public:
	CLock() = delete;
	CLock(std::string name) :m_name(name) {}
	void Lock();
	void Unlock();

};
