#pragma once
#include "DB_Module.h"



class CSession;
class ExOver;
class CDB_Module;

class CNetworkModule
{
protected:

	short m_port;
	HANDLE m_hIocp;
	SOCKET m_socket;
	std::atomic<bool> is_connect;

	std::vector<std::thread> workers;
	//CSession* m_sessionArr;
	std::atomic_int m_close_id;

	void worker(int id);

	CDB_Module m_DBarr[MAX_THREADS];
	std::thread test;
	CSession* m_GameServer;
public:

	CNetworkModule();
	virtual ~CNetworkModule();
	void Init();

	void onConnect();
	void run();

	void Disconnect(int key);// 접속 끊어버리기

	bool onRecv(int id, ExOver* over, int numData,CDB_Module& db);
	bool onSend(int id, ExOver* over, int numData);
	void process_packet(int id, unsigned char* packet,CDB_Module& db);

	void Disconnect_client();





};


