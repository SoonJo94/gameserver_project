#pragma once

#include <WinSock2.h>
#include <windows.h>
#include <string>
#include <iostream>
#pragma comment (lib, "ws2_32.lib")

#include <vector>
#include <mutex>
#include "../../MyGameServer/SeverEngine/include/DB_Protocol.h"

#include "DB_Tables.h"
const std::string SERVER_IP = "127.0.0.1";

constexpr int MAX_THREADS = 4;