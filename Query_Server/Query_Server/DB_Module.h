#pragma once

#include <sqlext.h>
class CDB_Module
{
	SQLHDBC m_hdc;
	SQLHENV m_henv;
	//SQLHSTMT hstmt;

public:

	CDB_Module();
	virtual ~CDB_Module();
	void DB_Connect(const std::wstring& DBname);

	DB_RESULT DB_LoginUser(const std::wstring& id, const std::wstring& password);

	/// <summary>
	/// 
	/// </summary>
	/// <param name="user_info"> In out ���� </param>
	/// <returns></returns>
	bool DB_LoadUserInfo(/* IN out */Table_User_info& user_info);
	bool DB_CreateUserInfo(const std::wstring& id,int x,int y);
	void HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode);
};

