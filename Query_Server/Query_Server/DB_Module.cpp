#include "stdafx.h"
#include "CNetworkModule.h"
#include "DB_Module.h"
#include "DB_Tables.h"
CDB_Module::CDB_Module() :m_hdc(), m_henv()
{
	//DB




}



CDB_Module::~CDB_Module()
{
	std::cout << "소멸\n";
	SQLDisconnect(m_hdc);
	SQLFreeHandle(SQL_HANDLE_DBC, m_hdc);
	SQLFreeHandle(SQL_HANDLE_ENV, m_henv);

}

void CDB_Module::DB_Connect(const std::wstring& DBname)
{
	SQLRETURN retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &m_henv);
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
	{
		retcode = SQLSetEnvAttr(m_henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, m_henv, &m_hdc);
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(m_hdc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);
				retcode = SQLConnect(m_hdc, (SQLWCHAR*)DBname.c_str(), SQL_NTS, (SQLWCHAR*)NULL, 0, NULL, 0);
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO)
					std::cout << "DBConnect\n";
				else
					HandleDiagnosticRecord(m_hdc, SQL_HANDLE_DBC, retcode);
			}
			else
				HandleDiagnosticRecord(m_hdc, SQL_HANDLE_DBC, retcode);

		}
		else
			HandleDiagnosticRecord(m_henv, SQL_HANDLE_ENV, retcode);
	}
	else
	{
		HandleDiagnosticRecord(m_henv, SQL_HANDLE_ENV, retcode);
	}
}

DB_RESULT CDB_Module::DB_LoginUser(const std::wstring& id, const std::wstring& password)
{
	int db_result = -1;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode = 0;


	retcode = SQLAllocHandle(SQL_HANDLE_STMT, m_hdc, &hstmt);

	std::wstring op = L"EXEC LoginUser " + id + L", " + password;
	SQLWCHAR szUser_name[USER_NAME_LEN] = { 0, };
	SQLINTEGER  dUser_idx = 0;
	SQLLEN cbName = 0, cbIdx = 0;

	retcode = SQLExecDirect(hstmt, (SQLWCHAR*)op.c_str(), SQL_NTS);//명령어 넣는부분
	if (retcode == SQL_SUCCESS) {
		retcode = SQLFetch(hstmt);
		SQLINTEGER ret{ 0 };
		SQLLEN intExits{ 0 };
		retcode = SQLGetData(hstmt, 1, SQL_INTEGER, &ret, 100, &intExits);
		if (retcode == SQL_SUCCESS) {
			/*	std::wcout << id << ": ";
				if (ret == 1)
					std::cout << " 로그인 성공!\n";
				else if (ret == 2)
					std::cout << "없어서 새로 만듬\n";
				else
					std::cout << "비밀번호 틀렸음\n";*/
			db_result = (DB_RESULT)ret;

		}
		else {

			HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
		}
	}
	else
		HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);


	return (DB_RESULT)db_result;
}



bool CDB_Module::DB_LoadUserInfo(Table_User_info& user_info)
{

	SQLHSTMT hstmt = 0;
	SQLRETURN retcode = 0;

	retcode = SQLAllocHandle(SQL_HANDLE_STMT, m_hdc, &hstmt);

	std::wstring op = L"EXEC LoadUserInfo " + user_info.name;
	SQLWCHAR szUser_name[USER_NAME_LEN] = { 0, };
	SQLINTEGER  dUser_idx = 0;
	SQLLEN cbName = 0, cbIdx = 0;

	retcode = SQLExecDirect(hstmt, (SQLWCHAR*)op.c_str(), SQL_NTS);//명령어 넣는부분
	if (retcode == SQL_SUCCESS) {
		retcode = SQLFetch(hstmt);
		if (retcode == SQL_SUCCESS) {
			SQLINTEGER X_pos{ 0 }, Y_pos{ 0 }, Max_hp{ 0 }, Dmg{ 0 }, Lv{ 0 }, CurHp{ 0 };
			SQLLEN intLen{ SQL_INTEGER };
			retcode = SQLGetData(hstmt, 1, SQL_INTEGER, &X_pos, 10, &intLen);
			retcode = SQLGetData(hstmt, 2, SQL_INTEGER, &Y_pos, 10, &intLen);
			retcode = SQLGetData(hstmt, 3, SQL_INTEGER, &Max_hp, 10, &intLen);
			retcode = SQLGetData(hstmt, 4, SQL_INTEGER, &Dmg, 10, &intLen);
			retcode = SQLGetData(hstmt, 5, SQL_INTEGER, &Lv, 10, &intLen);
			retcode = SQLGetData(hstmt, 6, SQL_INTEGER, &CurHp, 10, &intLen);
			user_info.x_pos = X_pos; user_info.y_pos = Y_pos; user_info.max_hp = Max_hp; user_info.dmg = Dmg;
			user_info.Lv = Lv; user_info.Cur_exp = CurHp;
		}

	}
	else {
		HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
		return false;
	}
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);


	return true;
}

bool CDB_Module::DB_CreateUserInfo(const std::wstring& id, int x, int y)
{
	int db_result = -1;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode = 0;

	retcode = SQLAllocHandle(SQL_HANDLE_STMT, m_hdc, &hstmt);

	std::wstring op = L"EXEC CreateUserInfo " + id + L", " + std::to_wstring(x) + L", " + std::to_wstring(y);
	SQLWCHAR szUser_name[USER_NAME_LEN] = { 0, };
	SQLINTEGER  dUser_idx = 0;
	SQLLEN cbName = 0, cbIdx = 0;

	retcode = SQLExecDirect(hstmt, (SQLWCHAR*)op.c_str(), SQL_NTS);//명령어 넣는부분
	if (retcode == SQL_SUCCESS) {

	}
	else {
		HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
		return false;
	}
	SQLFreeHandle(SQL_HANDLE_STMT, hstmt);


	return true;
}

/************************************************************************
/* HandleDiagnosticRecord : display error/warning information
/*
/* Parameters:
/*      hHandle     ODBC handle
/*      hType       Type of handle (HANDLE_STMT, HANDLE_ENV, HANDLE_DBC)
/*      RetCode     Return code of failing command
/************************************************************************/
void CDB_Module::HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{

	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];


	if (RetCode == SQL_INVALID_HANDLE)
	{
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}

	while (SQLGetDiagRec(hType,
		hHandle,
		++iRec,
		wszState,
		&iError,
		wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)),
		(SQLSMALLINT*)NULL) == SQL_SUCCESS)
	{
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5))
		{
			//fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
			std::wcout << wszState << ' ' << wszMessage << ' ' << iError;
		}
	}
}
