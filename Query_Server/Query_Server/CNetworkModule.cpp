#include "stdafx.h"

#include <atomic>
#include <mutex>
#include <random>

#include "CNetworkModule.h"

#include "Session.h"
#include "DB_Tables.h"


using namespace std::chrono;

void CNetworkModule::worker(int thread_id)
{
	auto& db = m_DBarr[thread_id];
	while (true)
	{
		DWORD numBytes;
		LONG64 key;
		//memset(&key, 0, sizeof(key));
		WSAOVERLAPPED* over;
		BOOL ret = GetQueuedCompletionStatus(m_hIocp, &numBytes, (PULONG_PTR)&key, &over, INFINITE);

		int id = static_cast<int>(key);
		ExOver* exOver = reinterpret_cast<ExOver*>(over);
		if (ret == FALSE)
		{
			int err = WSAGetLastError();
			if (err != ERROR_IO_PENDING) {

				std::cout << err << " GQCS err: ";
				Disconnect(id);
				continue;
			}
		}


		switch (exOver->m_op)
		{
		case IO_DB_RECV: {
			if (onRecv(id, exOver, numBytes, db) == false) {
				std::cout << "recv 오류 종료!\n";
				exit(-1);
			}
		}
					   break;
		case IO_DB_SEND:
			if (onSend(id, exOver, numBytes) == false) {
				std::cout << "send 오류 종료!\n";
				exit(-1);
			}
			break;
		default:
			std::cout << "잘못된 작업\n";
			break;
		}



	}
}

constexpr int CONNECT_DELAY = 50;// 접속 지연시간은 50ms
constexpr int LIMIT_DELAY = 1000;// 렉으기준은 1000ms; 1초
//constexpr int LIMIT2_DELAY = LIMIT_DELAY/2;// 렉이 생길려는 징조 접속속도느리게한다.



CNetworkModule::CNetworkModule()
{
}

CNetworkModule::~CNetworkModule()
{


	for (auto& th : workers)
		th.join();

	if (m_GameServer != nullptr)
	{
		delete m_GameServer;
		m_GameServer = nullptr;
	}


}

void CNetworkModule::Init()
{



	std::wcout.imbue(std::locale("korean"));
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	for (auto& db : m_DBarr)
		db.DB_Connect(L"GameServerDB");
	m_hIocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0); //iocp 커널객체생성
	onConnect();

}

void CNetworkModule::onConnect()
{
	int GameServerID = 0;
	m_GameServer = new CSession;
	m_GameServer->init();

	m_GameServer->SetId(GameServerID);
	m_GameServer->SetSock(WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED));

	SOCKADDR_IN sockAddr;
	memset(&sockAddr, 0, sizeof(sockAddr));
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_port = htons(SERVER_PORT);
	sockAddr.sin_addr.s_addr = inet_addr(SERVER_IP.c_str());

	int ret = WSAConnect(m_GameServer->GetSock(), (sockaddr*)&sockAddr, sizeof(sockaddr), NULL, NULL, NULL, NULL);
	if (ret != S_OK)
	{
		return;
	}

	CreateIoCompletionPort((HANDLE)m_GameServer->GetSock(), m_hIocp, GameServerID, 0);

	q2g_Connect_REQ packet;
	packet.size = sizeof(packet);
	packet.type = Protocol::Q2G_Connect_REQ;

	m_GameServer->do_send(&packet, packet.size);

	m_GameServer->do_recv();

}

void CNetworkModule::run()
{

	for (int i = 0; i < MAX_THREADS; ++i)
		workers.emplace_back(&CNetworkModule::worker, this, i);

}

void CNetworkModule::Disconnect(int key)
{
}



bool CNetworkModule::onRecv(int id, ExOver* over, int numData, CDB_Module& db)
{
	if (numData == 0) {
		return false;
	}
	CSession* session = m_GameServer;
	int prevSize = session->GetPrevSize(); //이전에 가지고있던 데이터 크기
	int remain_data = prevSize + numData;  //현재까지 recv된 총 패킷들의 크기의 합(잘릴 수도 있고, 모자랄 수도 있음)
	BYTE* packet_start = over->m_buf;
	int packet_size = 0; //처리해야하는 하나의 패킷크기
	memcpy(&packet_size, packet_start, sizeof(int));  //패킷에 써있는 헤더크기를 읽는다.


	while (packet_size <= remain_data)
	{
		process_packet(id, packet_start, db);
		remain_data -= packet_size;
		packet_start += packet_size;
		if (remain_data > 4) {
			memcpy(&packet_size, packet_start, sizeof(int));
		}
		else
			break;
	}
	if (remain_data > 0) {
		session->SetPrevSize(remain_data);
		//uint8_t* newPacket_start = session->GetBuf();
		memcpy(over->m_buf, packet_start, remain_data); //recv버퍼 맨앞으로 이동시켜준다
		//memset(packet_start, 0, sizeof(over->m_buf) - remain_data); //이거는 지울 수 도 있음, 쓰레기 값 안받으려고 해둠
	}
	session->do_recv();
	return true;
}

bool CNetworkModule::onSend(int id, ExOver* over, int numData)
{
	if (numData != over->m_wsaBuf.len) {
		delete over;
		return false;
	}
	delete over;
	return true;
}

void CNetworkModule::process_packet(int id, unsigned char* packet, CDB_Module& db)
{
	const BYTE* pData = packet;

	int type = 0;
	memcpy(&type, pData + 4, sizeof(int));  //패킷 첫번째 4바이트는 패킷의 크기이다.
	switch (type)
	{
	case Protocol::G2Q_Connect_OK:
	{

		is_connect = true;
		std::cout << "게임서버 연결 완료!\n";
		break;
	}
	case G2Q_LOGIN_REQ:
	{

		g2q_LOGIN_REQ* p = reinterpret_cast<g2q_LOGIN_REQ*>(packet);


		char name[USER_NAME_LEN];
		char password[USER_NAME_LEN];
		strcpy_s(name, p->name);
		std::string sID = name;
		std::wstring ID; ID.assign(sID.begin(), sID.end());
		strcpy_s(password, p->password);
		std::string sPass = password;
		std::wstring PASSWORD;
		PASSWORD.assign(sPass.begin(), sPass.end());


		DB_RESULT db_result = db.DB_LoginUser(ID, PASSWORD);

		switch (db_result)
		{
		case DB_LOGIN_FAIL:
		{
			q2g_LOGIN_RESULT pp;
			pp.size = sizeof(pp);
			pp.ret = db_result;
			pp.type = Q2G_LOGIN_RESULT;
			pp.x_pos = 0;
			pp.y_pos = 0;
			pp.id = p->id;
			m_GameServer->do_send(&pp, pp.size);

		}
		break;
		case DB_Login_OK:
		{
			Table_User_info user_info;
			user_info.name = ID;	
			db.DB_LoadUserInfo(user_info);
			q2g_LOGIN_RESULT pp;
			pp.size = sizeof(pp);
			pp.ret = db_result;
			pp.type = Q2G_LOGIN_RESULT;
			pp.x_pos = user_info.x_pos;
			pp.y_pos = user_info.y_pos;
			pp.id = p->id;
			pp.max_hp = user_info.max_hp;
			pp.dmg = user_info.dmg;
			pp.lv = user_info.Lv;
			pp.cur_exp = user_info.Cur_exp;
			m_GameServer->do_send(&pp, pp.size);
		}
		break;
		case DB_Create_ID:
		{
			int x = rand() % DB_WORLD_SIZE;
			int y = rand() % DB_WORLD_SIZE;
			db.DB_CreateUserInfo(ID, x, y);
			q2g_LOGIN_RESULT pp;
			pp.size = sizeof(pp);
			pp.ret = db_result;
			pp.type = Q2G_LOGIN_RESULT;
			pp.x_pos = x;
			pp.y_pos = y;
			pp.id = p->id;
			pp.max_hp = 100;
			pp.dmg = 10;
			pp.lv = 1;
			pp.cur_exp = 0;
			m_GameServer->do_send(&pp, pp.size);
		}
		break;
		default:
			break;
		}
		break;
	}
	default:
		break;
	}

}

void CNetworkModule::Disconnect_client()
{
	int closeid = m_close_id;
	bool status = true;
	//if (true == std::atomic_compare_exchange_strong(&m_sessionArr[closeid].isConnect, &status, false)) {
	//	closesocket(m_sessionArr[closeid].GetSock());
	//	m_active_clients--;
	//	m_close_id++;
	//}


}

