#include <iostream>
#include <WS2tcpip.h>//2는 버전임
#include "flatbuffers/flatbuffers.h"
#include "..\fbs\TEST_generated.h"

#pragma comment (lib, "WS2_32.LIB")
using namespace std;


const short SERVER_PORT = 4000;
void Send(SOCKET toSocket)
{ //write 하는 부분

	char buf[256];
	ZeroMemory(buf, sizeof(buf));
	flatbuffers::FlatBufferBuilder builder;
	auto msg = builder.CreateString("HelloWorld");
	auto data = fbs::CreateC2S_LOGIN_REQ(builder, 2020, msg);
	auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_LOGIN_REQ, data.Union());

	builder.Finish(root);

	cout << "보내는 거 사이즈:" << builder.GetSize() << '\n';

	memcpy(buf, builder.GetBufferPointer(), builder.GetSize());

	WSABUF mybuf;
	mybuf.buf = buf;
	mybuf.len = builder.GetSize();
	DWORD sent_bytes;
	WSASend(toSocket, &mybuf, 1, &sent_bytes, 0, 0, 0);
}

void Send(SOCKET toSocket,const string& message)
{ //write 하는 부분

	char buf[256];
	ZeroMemory(buf, sizeof(buf));
	flatbuffers::FlatBufferBuilder builder;
	auto msg = builder.CreateString(message);
	auto data = fbs::CreateC2S_LOGIN_REQ(builder, 2020, msg);
	auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_LOGIN_REQ, data.Union());

	builder.Finish(root);

	cout << "보내는 거 사이즈:" << builder.GetSize() << '\n';

	memcpy(buf, builder.GetBufferPointer(), builder.GetSize());

	WSABUF mybuf;
	mybuf.buf = buf;
	mybuf.len = builder.GetSize();
	DWORD sent_bytes;
	WSASend(toSocket, &mybuf, 1, &sent_bytes, 0, 0, 0);
}


void Recv(SOCKET fromSocket)
{

	char recvBuf[256];
	WSABUF mybuf;
	mybuf.buf = recvBuf;
	mybuf.len = 256;
	DWORD recv_bytes;
	DWORD recv_flag = 0;
	WSARecv(fromSocket, &mybuf, 1, &recv_bytes, &recv_flag, 0, 0);


	{//read 하는 부분
		const uint8_t* pData = (const uint8_t*)recvBuf;
		auto verifier = flatbuffers::Verifier(pData, recv_bytes);
		if (false == fbs::VerifyRootBuffer(verifier)) {
			cout << "패킷다안온거임 쯧쯧\n";
			return;
		}
		auto root = fbs::GetRoot(pData);
		auto type = root->packet_type();
		switch (type)
		{
		case fbs::Packet_NONE:
			cout << "잘못된 패킷 도착!\n";
			break;
		case fbs::Packet_C2S_LOGIN_REQ:
		{
			auto packet = root->packet_as_C2S_LOGIN_REQ();

			string msg = packet->msg()->data();
			cout << "[받은 데이터:" << recv_bytes << "bytes] " << msg << ' ' << packet->value() << "\n";

			Send(fromSocket, msg);
		}
		break;
		case fbs::Packet_S2C_LOGIN_OK:
			break;
		default:
			break;
		}
	}
}


int main()
{


	//C#으로 해당 버퍼 send해보자.

	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 0), &wsaData);//윈도우에서는 호환성때문에 이것을 사용해야한다.
	SOCKET serverSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, 0);
	SOCKADDR_IN server_addr;
	ZeroMemory(&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(SERVER_PORT);
	server_addr.sin_addr.s_addr = INADDR_ANY;
	::bind(serverSocket, reinterpret_cast<sockaddr*>(&server_addr), sizeof(server_addr));
	listen(serverSocket, SOMAXCONN);

	INT addr_size = sizeof(server_addr);
	SOCKET client_socket = WSAAccept(serverSocket, reinterpret_cast<sockaddr*>(&server_addr), &addr_size, 0, 0);
	cout << "누군가 연결했다!\n";
	for (;;) {


		Recv(client_socket);
		//Send(client_socket);


	}
	closesocket(serverSocket);
	closesocket(client_socket);
	WSACleanup();


}