#include <iostream>
#include "flatbuffers/flatbuffers.h"
#include "..\fbs\TEST_generated.h"
using namespace std;

char buf[256];
int remainData = 0;
char* startBuf = nullptr;
int bufOffset = 0;
int recvData = 0;


void WriteNumByte(const flatbuffers::FlatBufferBuilder& builder, int size)
{



	memcpy(buf + bufOffset, builder.GetBufferPointer(), size);
	bufOffset += size;
	recvData += size;
	//memset(buf + 40, 0, 20);


}

void Read()
{//read 

	
	while (true)
	{
		flatbuffers::uoffset_t packetSize = startBuf[0]+4;
		if (packetSize > bufOffset) {
			memcpy(buf, startBuf, bufOffset); //남은 크기 만큼 앞으로 땡기기 
			startBuf = buf;
			cout << "끝!" << '\n';
			return;
		}
		if (bufOffset <= 0)
		{
			bufOffset = 0;
			cout << "끝!" << '\n';
			return;
		}
		const uint8_t* pData = (const uint8_t*)startBuf;
		startBuf += packetSize;
		bufOffset -= packetSize;
		//auto verifier = flatbuffers::Verifier(pData, packetSize);
		//if (false == fbs::VerifySizePrefixedRootBuffer(verifier)) {
		//	cout << "패킷다안온거임 쯧쯧\n";
		//	return;
		//}

		auto root = fbs::GetSizePrefixedRoot(pData);
		auto type = root->packet_type();
		switch (type)
		{
		case fbs::Packet_NONE:
			break;
		case fbs::Packet_C2S_LOGIN_REQ:
		{
			auto packet = root->packet_as_C2S_LOGIN_REQ();
			cout << packet->msg()->data() << "\n";
			cout << packet->value() << "\n"; 
		}
		break;
		case fbs::Packet_S2C_LOGIN_OK:
			break;
		default:
			break;
		}

	}


}


int main()
{
	startBuf = buf;


	{
		flatbuffers::FlatBufferBuilder builder;
		auto msg = builder.CreateString("HelloWorld");
		auto data = fbs::CreateC2S_LOGIN_REQ(builder, 2020, msg);
		auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_LOGIN_REQ, data.Union());
		builder.FinishSizePrefixed(root);

		cout << "send size:" <<builder.GetSize()<< '\n';
		WriteNumByte(builder, builder.GetSize());
	}

	{
		flatbuffers::FlatBufferBuilder builder;
		//builder.Clear();
		auto msg = builder.CreateString("HelloWorld");
		auto data = fbs::CreateC2S_LOGIN_REQ(builder, 2022, msg);
		auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_LOGIN_REQ, data.Union());

		builder.FinishSizePrefixed(root);

		cout << "send size:" << builder.GetSize() << '\n';
		WriteNumByte(builder, builder.GetSize());
	}

	{
		flatbuffers::FlatBufferBuilder builder;
		//builder.Clear();
		auto msg = builder.CreateString("You can doddd it!");
		auto data = fbs::CreateC2S_LOGIN_REQ(builder, 2022, msg);
		auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_LOGIN_REQ, data.Union());

		builder.FinishSizePrefixed(root);

		cout << "send size:" << builder.GetSize() << '\n';
		WriteNumByte(builder, builder.GetSize());
	}

	Read(); //한번에 하나만 읽는다.

	//Read();
	//Read();
   
}