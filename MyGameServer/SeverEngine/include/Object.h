#pragma once
class CObject //최상위 부모 클래스
{
protected:
	bool m_active;
	int m_x_pos;
	int m_y_pos;
	int m_maxHp;
	int m_curHp;
public:
	CObject();
	virtual ~CObject();
	/// <summary>
	/// 데미지 입는 함수
	/// </summary>
	/// <param name="dmg"></param>
	/// <returns>현재체력 반환 </returns>
	virtual int Hitted(int dmg)=0;
	bool Active() { return m_active; }
	void SetActive(bool isActive) { m_active = isActive; }
	void GetHps(int& max, int& cur);
	POINT GetPosition() { return { m_x_pos,m_y_pos }; }
};
