#pragma once
#include "Object.h"

enum IO_OP
{
	IO_READ,
	IO_WRITE,
	IO_ACCEPT,
	IO_DB_RECV,
	IO_DB_SEND,
	IO_DB_ACCEPT,
	IO_OTHER //다른거 할 꺼 
};


class Over
{
public:
	WSAOVERLAPPED m_over;
	IO_OP	m_op;
	int m_other_op;
	Over() {};
	~Over() {};
};

class ExOver : public Over
{
public:

	BYTE	m_buf[BUFSIZE];
	WSABUF	m_wsaBuf;
	SOCKET	m_socket;
	int m_prev_size = 0;
public:
	ExOver()
	{
		m_op = IO_OP::IO_READ;
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf);
		m_wsaBuf.len = sizeof(m_buf);
		memset(&m_over, 0, sizeof(m_over));
		m_prev_size = 0;
	}
	void init()
	{
		m_op = IO_OP::IO_READ;
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf);
		m_wsaBuf.len = sizeof(m_buf);
		memset(&m_over, 0, sizeof(m_over));
		memset(m_buf, 0, sizeof(m_buf));
		m_prev_size = 0;

	}
	void do_recv()
	{
		m_op = IO_READ;
		memset(&m_over, 0, sizeof(m_over));
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf + m_prev_size);
		//m_wsaBuf.len = sizeof(m_buf) - m_prev_size; 아래걸로 바뀜 이렇게하면 안된다.
		m_wsaBuf.len = BUFSIZE - m_prev_size;

		DWORD flag = 0;
		int ret = WSARecv(m_socket, &m_wsaBuf, 1, 0, &flag, &m_over, NULL);
		if (SOCKET_ERROR == ret) {
			int error_num = WSAGetLastError();
			if (ERROR_IO_PENDING != error_num)
				error_display(error_num);
		}

	}
	void do_send(void* data, int num_bytes)
	{
		char* t = reinterpret_cast<char*>(data);
		/*if (t[1] == 0) {
			std::cout << "오류!\n";
		}*/
		ExOver* over = new ExOver();
		memset(&over->m_over, 0, sizeof(over->m_over));


		over->m_wsaBuf.buf = reinterpret_cast<char*>(over->m_buf);
		over->m_wsaBuf.len = num_bytes;
		over->m_op = IO_WRITE;
		DWORD flag = 0;
		memcpy(over->m_buf, data, num_bytes);
		int ret = WSASend(m_socket, &over->m_wsaBuf, 1, 0, 0, &over->m_over, NULL);
		if (SOCKET_ERROR == ret) {
			int error_num = WSAGetLastError();
			if (ERROR_IO_PENDING != error_num) {
				error_display(error_num);

			}

		}
	}

	bool do_DBrecv()
	{
		m_op = IO_DB_RECV;
		memset(&m_over, 0, sizeof(m_over));
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf + m_prev_size);
		//m_wsaBuf.len = sizeof(m_buf) - m_prev_size; 아래걸로 바뀜 이렇게하면 안된다.
		m_wsaBuf.len = BUFSIZE - m_prev_size;

		DWORD flag = 0;
		int ret = WSARecv(m_socket, &m_wsaBuf, 1, 0, &flag, &m_over, NULL);
		if (SOCKET_ERROR == ret) {
			int error_num = WSAGetLastError();
			if (ERROR_IO_PENDING != error_num)
				return false;
		}
		return true;

	}
	void do_DBsend(void* data, int num_bytes)
	{

		ExOver* over = new ExOver();
		memset(&over->m_over, 0, sizeof(over->m_over));


		over->m_wsaBuf.buf = reinterpret_cast<char*>(over->m_buf);
		over->m_wsaBuf.len = num_bytes;
		over->m_op = IO_DB_SEND;
		DWORD flag = 0;
		memcpy(over->m_buf, data, num_bytes);
		int ret = WSASend(m_socket, &over->m_wsaBuf, 1, NULL, 0, &over->m_over, NULL);
		if (SOCKET_ERROR == ret) {
			int error_num = WSAGetLastError();
			/*		if (ERROR_IO_PENDING != error_num)
						error_display(error_num);*/
		}
	}
	~ExOver()
	{
		//std::cout << "ExOver 소멸~~\n";
	}
};

enum STATE
{
	ST_FREE, ST_ACCEPT, ST_INGAME
};

struct CSessionInfo
{
	ExOver* over;
	int id;

};

class CSession :public CObject
{
protected:
	ExOver m_exOver;
	int m_id;
	std::mutex m_sLock; //세션락
	STATE m_eState;

	int prev_data;//처리해야할 데이터

	std::chrono::system_clock::time_point m_connectTime;

public:
	CSession() :m_eState(ST_FREE)
	{

	}
	int GetId() { return m_id; }
	void S_Lock() { m_sLock.lock(); }
	void S_Unlock() { m_sLock.unlock(); }
	void SetId(int id) { m_id = id; }
	int ID() { return m_id; }
	STATE State() { return m_eState; }
	void SetState(STATE s) { m_eState = s; }
	void SetSock(SOCKET s) { m_exOver.m_socket = s; }
	SOCKET GetSock() { return m_exOver.m_socket; }
	virtual void  init();
	virtual void init(int id);
	void do_recv() { m_exOver.do_recv(); }
	int GetPrevSize() { return m_exOver.m_prev_size; }
	void SetPrevSize(int size) { m_exOver.m_prev_size = size; }
	void do_send(void* data, int numBytes);
	void do_dbSend(void* data, int numBytes);
	void do_dbRecv();
	BYTE* GetBuf() { return m_exOver.m_buf; }

	void UpdateConnectTime() { std::lock_guard<std::mutex> lockGuard(m_sLock); m_connectTime = std::chrono::system_clock::now(); }
	const std::chrono::system_clock::time_point& GetConnectTime() { return m_connectTime; }

};

