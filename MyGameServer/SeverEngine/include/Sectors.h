#pragma once

#include <unordered_set>

class CSectors //멀티 쓰레드 전에 생성할것!
{
	int m_sectorWidth;
	using Sector = std::unordered_set<int>;
	Sector** m_ppSectors;// id를 가지고 있는 set의 2차원 배열

	//std::array<std::array<const Sector*, 9>, fbs::Constants::Constants_MAX_USER+fbs::Constants_MAX_NPC> m_userSectorArray; 
	std::vector<std::array<const Sector*, 9>> m_userSectorArray; 
	std::mutex m_sectorLock;
public:
	CSectors()=delete;
	CSectors(int worldSize);
	virtual ~CSectors();

	void init(int worldSize);
	void updateSector(int x, int y, int newX, int newY, int id); //해당 ID에 섹터 정보들을 리턴
	void InsertObject(int x, int y, int id);
	void deleteObject( int id);
	const std::array<const CSectors::Sector*, 9>& GetSectors(int id); //자기포함 주변 8방향(좌상,상, 우상, 좌, 본인,우,좌하,하,좌하)의 섹터

};

