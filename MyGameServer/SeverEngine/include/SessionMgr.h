#pragma once

class CObjectMgr :public CSingleton<CObjectMgr>
{


	std::array<CObject*, fbs::Constants::Constants_MAX_USER + fbs::Constants::Constants_MAX_NPC> m_objectArr;//이것을 굳이 인터페이스를뺄필요가있나?
public:

	//const int  MAX_USER = 2000;
	//const int  MAX_NPC = 0;
	//const int NPC_ID_START = MAX_USER;
	//const int NPC_ID_END = MAX_USER + MAX_NPC - 1;



	std::array<CObject*, fbs::Constants::Constants_MAX_USER + fbs::Constants::Constants_MAX_NPC>& ObjectArr() { return m_objectArr; };

	CObject* GetObjectPtr(int id);
	CSession* GetSession(int id);
	CSessionMgr();
	~CSessionMgr();


};

