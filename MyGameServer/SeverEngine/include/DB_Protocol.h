#pragma once

constexpr int USER_NAME_LEN = 20;
constexpr short SERVER_PORT = 4000;
constexpr int DB_WORLD_SIZE = 400;
enum Protocol:int
{
	//GameServer To QueryServer= G2Q

	Q2G_Connect_REQ,
	G2Q_Connect_OK,
	G2Q_LOGIN_REQ, 
	Q2G_LOGIN_RESULT
};

enum DB_RESULT :int
{
	DB_LOGIN_FAIL=-1,
	DB_OK,
	DB_Login_OK=1,
	DB_Create_ID=2,
};

#pragma pack (push, 1)
struct q2g_Connect_REQ
{
	int size;
	Protocol type;
};
struct g2q_Connect_OK
{
	int size;
	Protocol type;
};

struct g2q_LOGIN_REQ
{
	int size;
	Protocol type;
	int id;
	char name[USER_NAME_LEN];
	char password[USER_NAME_LEN];

};


struct q2g_LOGIN_RESULT
{
	int size;
	Protocol type;
	int id;
	int x_pos=0;
	int y_pos=0;
	int max_hp;
	int dmg;
	int lv;
	int cur_exp;
	DB_RESULT ret;
};


#pragma pack(pop)