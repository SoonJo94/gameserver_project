#include "stdafx.h"
#include "Session.h"

CSession::CSession(int id) :m_eState(ST_FREE)
{
	init(id);
}

void CSession::init()
{
	m_id = -1;
	isConnect = false;
	m_exOver.init();
}

void CSession::init(int id)
{
	isConnect = false;
	m_exOver.init();
}

void CSession::do_send(void* data, int numBytes)
{
	m_exOver.do_send(data, numBytes);
}

void CSession::SetPos(int x, int y)
{
	m_x = x, m_y = y;
}
