#pragma once




class CSession;
class ExOver;

class CNetworkModule
{
protected:

	short m_port;
	HANDLE m_hIocp;
	SOCKET m_socket;
	bool is_running;

	std::vector<std::thread> workers;
	//CSession* m_sessionArr;
	std::atomic_int m_num_connect;  //어디까지 연결되었는지확인
	std::atomic_int m_active_clients;
	std::atomic_int m_close_id;
	std::atomic_int client_close_index; //어디까지 접속 끊었는지 확인
	int m_g_delay;

	std::chrono::high_resolution_clock::time_point m_last_connect_time;
	std::array<int, MAX_CLIENT> client_mappingID;
	void worker();
	std::thread test;
	void connect_client();
	void test_thread();
	

public:
	static std::array<CSession, MAX_CLIENT> m_sessionArr;
	CNetworkModule();
	virtual ~CNetworkModule();
	void Init();

	void onConnect(ExOver* over);
	void run();
	bool IsNpc(int id);
	bool IsPlayer(int id);
	void Disconnect(int key);// 접속 끊어버리기
	int get_new_id();
	bool onRecv(int id, ExOver* over, int numData);
	bool onSend(int id, ExOver* over, int numData);
	void process_packet(int id, unsigned char* packet);

	int get_delay() { return m_g_delay; }
	int get_active_client() { return m_active_clients; }
	void Disconnect_client();


	void OtherWork(int id, int op);


};


