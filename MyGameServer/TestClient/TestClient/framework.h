﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once
#include "stdafx.h"
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일

#include <WinSock2.h>
#include <windows.h>
// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <thread>
#include <string>
#include <iostream>
#include <mutex>	
#include <array>

#include "SJ_SEVER_ENGINE\Protocol_generated.h"
#include "SJ_SEVER_ENGINE\\Packet_generated.h"


#pragma comment (lib, "ws2_32.lib")


static const int MAX_USER = fbs::Constants::Constants_MAX_USER;
static const int MAX_CLIENT = MAX_USER * 2;  //클라 수가 변동하기 때문이다.
static const int MAX_NPC = fbs::Constants::Constants_MAX_NPC;
static const int NPC_ID_START = MAX_USER;
static const int NPC_ID_END = MAX_USER + MAX_NPC - 1;
static const int WORLD_SIZE = fbs::Constants::Constants_WORLD_SIZE;
static const short SERVER_PORT = fbs::Constants::Constants_SEVER_PORT;
const std::string SERVER_IP = "127.0.0.1";

constexpr int MAX_THREADS = 4;