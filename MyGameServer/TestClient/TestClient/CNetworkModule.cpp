#include "stdafx.h"
#include <atomic>
#include <mutex>
#include <random>
#include "CNetworkModule.h"

#include "Session.h"

using namespace std::chrono;

void CNetworkModule::worker()
{

	while (true)
	{
		DWORD numBytes;
		LONG64 key;
		//memset(&key, 0, sizeof(key));
		WSAOVERLAPPED* over;
		BOOL ret = GetQueuedCompletionStatus(m_hIocp, &numBytes, (PULONG_PTR)&key, &over, INFINITE);

		int id = static_cast<int>(key);
		ExOver* exOver = reinterpret_cast<ExOver*>(over);
		if (ret == FALSE)
		{
			int err = WSAGetLastError();
			if (err != ERROR_IO_PENDING) {

				std::cout << err << " GQCS err: ";
				Disconnect(id);
				continue;
			}
		}


		switch (exOver->m_op)
		{
		case IO_READ: {
			if (onRecv(id, exOver, numBytes) == false)
				Disconnect(id);
		}
					break;
		case IO_WRITE:
			if (onSend(id, exOver, numBytes) == false)
				Disconnect(id);
			break;
		default:
			OtherWork(id, exOver->m_op);
			break;
		}



	}
}

constexpr int CONNECT_DELAY = 50;// 접속 지연시간은 50ms
constexpr int LIMIT_DELAY = 1000;// 렉으기준은 1000ms; 1초
//constexpr int LIMIT2_DELAY = LIMIT_DELAY/2;// 렉이 생길려는 징조 접속속도느리게한다.

void CNetworkModule::connect_client()
{
	static int connect_time_value = 1;
	//동접 끝까지 차면 접속 끊기 잠깐만 그러면 그걸로 된거아냐? 뭘 끊는것 까지 해야되나? 내가 원하는 동접까지 받으면 그걸로 테스트하면 되는거 아님? 걍종료
	if (m_active_clients >= MAX_USER) return;
	//배열 범위 벗어나면 접속 끊기
	if (m_num_connect >= MAX_CLIENT) return;

	if (m_active_clients == 0)
		m_g_delay = 0;

	//동접 안꽉찼다고 막 접속시키면안됨 어느정도 텀을 두어야함
	int dt = duration_cast<milliseconds>(high_resolution_clock::now() - m_last_connect_time).count();

	if (CONNECT_DELAY > dt) return;
	//delay 검사
	int delay = m_g_delay;//멀티쓰레드이기에 이렇게 함
	if (delay >= LIMIT_DELAY) {//렉이걸린다 	//delay가 일정 수준 이상이면 접속 끊어야함

		m_last_connect_time = high_resolution_clock::now();
		//접속끊기
		Disconnect_client();
		return;
	}

	else if (delay >= LIMIT_DELAY / 2) {//어느정도 징조가있으면 접속 딜레이를 늘려야겠다.
		connect_time_value = 20;
	}
	else {
		connect_time_value = connect_time_value < 2 ? 1 : connect_time_value - 1;
	}

	if (CONNECT_DELAY * connect_time_value > dt) return;
	//현재시간 업데이트
	m_last_connect_time = high_resolution_clock::now();
	CSession& client = m_sessionArr[m_num_connect];

	client.init();
	client.SetId(m_num_connect);
	client.SetSock(WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED));


	SOCKADDR_IN sockAddr;
	memset(&sockAddr, 0, sizeof(sockAddr));
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_port = htons(SERVER_PORT);
	sockAddr.sin_addr.s_addr = inet_addr(SERVER_IP.c_str());


	int ret = WSAConnect(client.GetSock(), (sockaddr*)&sockAddr, sizeof(sockaddr), NULL, NULL, NULL, NULL);
	if (ret != S_OK)
	{
		return;
	}

	CreateIoCompletionPort((HANDLE)client.GetSock(), m_hIocp, m_num_connect, 0);


	//로그인요청 패킷보내기
	flatbuffers::FlatBufferBuilder builder;
	std::string testID = "Test" + std::to_string(m_num_connect);
	std::string password = "a";
	auto id = builder.CreateString(testID);
	auto pass = builder.CreateString(password);
	auto data = fbs::CreateC2S_LOGIN_REQ(builder, id, pass);
	auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_LOGIN_REQ, data.Union());
	builder.FinishSizePrefixed(root);
	client.do_send(builder.GetBufferPointer(), builder.GetSize());
	if (true == client.do_recv())
		m_num_connect++;


}

void CNetworkModule::test_thread()
{
	while (true) //계속 접속
	{
		connect_client();

		//자동이동 패킷 보내야한다.


		for (int i = 0; i < m_num_connect; ++i)
		{
			if (false == m_sessionArr[i].isConnect) continue;
			if (m_sessionArr[i].m_move_time + 1s > high_resolution_clock::now()) continue; //마지막 움직이고 1초 안지나면 pass
			m_sessionArr[i].m_move_time = high_resolution_clock::now();

			fbs::Direction dir[] = { fbs::Direction::Direction_Up,
									 fbs::Direction::Direction_Down,
									 fbs::Direction::Direction_Left ,
									fbs::Direction::Direction_Right };
			int movetime = duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count(); //현재 절대시간 보내기

			int index = rand() % 4;
			flatbuffers::FlatBufferBuilder builder;
			auto data = fbs::CreateC2S_Move_REQ(builder, dir[index], movetime);
			auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_Move_REQ, data.Union());
			builder.FinishSizePrefixed(root);
			m_sessionArr[i].do_send(builder.GetBufferPointer(), builder.GetSize());

		}

	}
}

CNetworkModule::CNetworkModule() :m_g_delay(0), m_active_clients(0)
{
}

CNetworkModule::~CNetworkModule()
{

	if (is_running == true) {
		for (auto& th : workers)
			th.join();
		test.join();
	}

}

void CNetworkModule::Init()
{

	for (int& id : client_mappingID)
		id = -1;
	m_num_connect = 0; //현재 연결 수
	m_active_clients = 0;
	m_last_connect_time = std::chrono::high_resolution_clock::now();

	std::wcout.imbue(std::locale("korean"));
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	m_hIocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0); //iocp 커널객체생성



	run(); //워커 쓰레드 작동
}

void CNetworkModule::onConnect(ExOver* over)
{
}

void CNetworkModule::run()
{

	is_running = true;
	for (int i = 0; i < MAX_THREADS; ++i)
		workers.emplace_back(&CNetworkModule::worker, this);

	test = std::thread{ &CNetworkModule::test_thread,this };


}

bool CNetworkModule::IsNpc(int id)
{
	return false;
}

bool CNetworkModule::IsPlayer(int id)
{
	return false;
}

void CNetworkModule::Disconnect(int key)
{
}

int CNetworkModule::get_new_id()
{
	return 0;
}

bool CNetworkModule::onRecv(int id, ExOver* over, int numData)
{
	if (numData == 0) {
		return false;
	}
	CSession* session = &m_sessionArr[id];
	int prevSize = session->GetPrevSize(); //이전에 가지고있던 데이터 크기
	int remain_data = prevSize + numData;  //현재까지 recv된 총 패킷들의 크기의 합(잘릴 수도 있고, 모자랄 수도 있음)
	uint8_t* packet_start = over->m_buf;
	flatbuffers::uoffset_t packet_size = 0; //처리해야하는 하나의 패킷크기
	memcpy(&packet_size, packet_start, sizeof(flatbuffers::uoffset_t));  //패킷에 써있는 헤더크기를 읽는다.
	packet_size += sizeof(flatbuffers::uoffset_t); //원래 패킷데이터크기 + prefix헤더의 크기를 더한다. 그것이 recv한 패킷하나의의 총 크기이다.


	while (packet_size <= remain_data)
	{
		process_packet(id, packet_start);
		remain_data -= packet_size;
		packet_start += packet_size;
		if (remain_data > 4) {
			memcpy(&packet_size, packet_start, sizeof(flatbuffers::uoffset_t)); //이부분이 문제일수도 있다.
			packet_size += sizeof(flatbuffers::uoffset_t);
		}
		else
			break;
	}
	if (remain_data > 0) {
		session->SetPrevSize(remain_data);
		//uint8_t* newPacket_start = session->GetBuf();
		memcpy(over->m_buf, packet_start, remain_data); //recv버퍼 맨앞으로 이동시켜준다
		//memset(packet_start, 0, sizeof(over->m_buf) - remain_data); //이거는 지울 수 도 있음, 쓰레기 값 안받으려고 해둠
	}
	session->do_recv();
	return true;
}

bool CNetworkModule::onSend(int id, ExOver* over, int numData)
{
	if (numData != over->m_wsaBuf.len) {
		delete over;
		return false;
	}
	delete over;
	return true;
}

void CNetworkModule::process_packet(int id, unsigned char* packet)
{
	const uint8_t* pData = (const uint8_t*)packet;
	auto root = fbs::GetSizePrefixedRoot(pData);
	auto type = root->packet_type();
	switch (type)  //스위치 케이스
	{
	case fbs::Packet_NONE:
		break;
	case fbs::Packet_S2C_LOGIN_RESULT:
	{
		auto packet = root->packet_as_S2C_LOGIN_RESULT();
		if (packet->result() != -1) {
			m_sessionArr[id].isConnect = true;
			m_active_clients++;



			int my_id = id;
			client_mappingID[packet->id()] = id;
			CSession& client = m_sessionArr[my_id];
			client.SetId(my_id);
			client.SetPos(packet->x_pos(), packet->y_pos());
			flatbuffers::FlatBufferBuilder builder;
			auto data = fbs::CreateC2S_Enter_GAME(builder,0);
			auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_Enter_GAME, data.Union());
			builder.FinishSizePrefixed(root);
			client.do_send(builder.GetBufferPointer(), builder.GetSize());

		}
		else {
			m_num_connect--;
		}
	

	}
	break;

	case fbs::Packet_S2C_Object_Move:
	{

		auto packet = root->packet_as_S2C_Object_Move();
		int packet_id = packet->id();
		int my_id = client_mappingID[packet_id];
		int px = packet->x_pos(), py = packet->y_pos();
		int p_time = packet->move_time();
		if (my_id != -1) //내가 접속시킨 클라이언트는 반영안시킴 
		{
			m_sessionArr[my_id].SetPos(px, py);
		}
		if (id == my_id)
		{
			if (0 != p_time) {
				auto dt = duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count() - p_time;
				if (m_g_delay < dt)m_g_delay++;
				else if (m_g_delay > dt)m_g_delay--;

			}
		}
	}
	break;
	case fbs::Packet_C2S_Enter_GAME:	break;
	case fbs::Packet_C2S_Chatt_Message:	break;
	default:
		/*while (true);
		std::cout << "wrong packet!\n";*/
		break;
	}

}

void CNetworkModule::Disconnect_client()
{
	int closeid = m_close_id;
	bool status = true;
	if (true == std::atomic_compare_exchange_strong(&m_sessionArr[closeid].isConnect, &status, false)) {

		flatbuffers::FlatBufferBuilder builder;
		auto data = fbs::CreateC2S_Disconnect(builder, closeid);
		auto root = fbs::CreateRoot(builder, fbs::Packet_C2S_Disconnect, data.Union());
		builder.FinishSizePrefixed(root);
		m_sessionArr[m_close_id].do_send(builder.GetBufferPointer(), builder.GetSize());

		closesocket(m_sessionArr[closeid].GetSock());
		m_active_clients--;
		m_close_id++;
	}


}

void CNetworkModule::OtherWork(int id, int op)
{
}
