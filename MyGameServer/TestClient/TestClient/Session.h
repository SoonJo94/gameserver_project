#pragma once


enum IO_OP
{
	IO_READ,
	IO_WRITE,
	IO_CONNECT
};

class CObject //최상위 부모 클래스
{

};

constexpr int BUFSIZE = 1024;
class ExOver
{
public:
	WSAOVERLAPPED m_over;
	IO_OP	m_op;
	BYTE	m_buf[BUFSIZE];
	WSABUF	m_wsaBuf;
	SOCKET	m_socket;
	int m_prev_size = 0;
public:
	ExOver()
	{
		m_op = IO_OP::IO_READ;
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf);
		m_wsaBuf.len = sizeof(m_buf);
		memset(&m_over, 0, sizeof(m_over));
		m_prev_size = 0;
	}
	void init()
	{
		m_op = IO_OP::IO_READ;
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf);
		m_wsaBuf.len = sizeof(m_buf);
		memset(&m_over, 0, sizeof(m_over));
		memset(m_buf, 0, sizeof(m_buf));
		m_prev_size = 0;

	}
	bool do_recv()
	{
		m_op = IO_READ;
		memset(&m_over, 0, sizeof(m_over));
		m_wsaBuf.buf = reinterpret_cast<char*>(m_buf + m_prev_size);
		//m_wsaBuf.len = sizeof(m_buf) - m_prev_size; 아래걸로 바뀜 이렇게하면 안된다.
		m_wsaBuf.len = BUFSIZE - m_prev_size;

		DWORD flag = 0;
		int ret = WSARecv(m_socket, &m_wsaBuf, 1, 0, &flag, &m_over, NULL);
		if (SOCKET_ERROR == ret) {
			int error_num = WSAGetLastError();
			if (ERROR_IO_PENDING != error_num)
				return false;
		}
		return true;

	}
	void do_send(void* data, int num_bytes)
	{
		char* t = reinterpret_cast<char*>(data);
		/*if (t[1] == 0) {
			std::cout << "오류!\n";
		}*/
		ExOver* over = new ExOver();
		memset(&over->m_over, 0, sizeof(over->m_over));


		over->m_wsaBuf.buf = reinterpret_cast<char*>(over->m_buf);
		over->m_wsaBuf.len = num_bytes;
		over->m_op = IO_WRITE;
		DWORD flag = 0;
		memcpy(over->m_buf, data, num_bytes);
		int ret = WSASend(m_socket, &over->m_wsaBuf, 1, NULL, 0, &over->m_over, NULL);
		if (SOCKET_ERROR == ret) {
			int error_num = WSAGetLastError();
			/*		if (ERROR_IO_PENDING != error_num)
						error_display(error_num);*/
		}
	}
	~ExOver()
	{
		//std::cout << "ExOver 소멸~~\n";
	}
};

enum STATE
{
	ST_FREE, ST_ACCEPT, ST_INGAME
};

struct CSessionInfo
{
	ExOver* over;
	int id;

};
class CSession :public CObject
{
protected:
	ExOver m_exOver;
	int m_id;
	std::mutex m_sLock; //세션락
	STATE m_eState;

	int prev_data;//처리해야할 데이터
	int m_x, m_y;


public:
	std::chrono::high_resolution_clock::time_point m_move_time; //매프레임마다 보내지않게하기위해서

	std::atomic_bool isConnect;
	CSession() :m_eState(ST_FREE)
	{
		init();
	}

	CSession(int id);
	int GetId() { return m_id; }

	void S_Lock() { m_sLock.lock(); }
	void S_Unlock() { m_sLock.unlock(); }
	void SetId(int id) { m_id = id; }
	int ID() { return m_id; }
	STATE State() { return m_eState; }
	void SetState(STATE s) { m_eState = s; }
	void SetSock(SOCKET s) { m_exOver.m_socket = s; }
	SOCKET GetSock() { return m_exOver.m_socket; }
	void  init();
	void init(int id);
	bool do_recv() { return m_exOver.do_recv(); }
	int GetPrevSize() { return m_exOver.m_prev_size; }
	void SetPrevSize(int size) { m_exOver.m_prev_size = size; }
	void do_send(void* data, int numBytes);
	
	void SetPos(int x, int y);
	void GetPos(int& x, int& y) { x = m_x; y = m_y; }




	BYTE* GetBuf() { return m_exOver.m_buf; }

};

