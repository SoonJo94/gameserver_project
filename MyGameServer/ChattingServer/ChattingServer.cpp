#include "stdafx.h"

#include "Timer_event.h"

#include "ChattingServer.h"
#include "Player.h"
#include "Monster.h"
#include "OtherWorkOp.h"


CChattingServer::CChattingServer() :m_sectors(WORLD_SIZE)
{
	initChattingServer();
	m_pTimer = new CTimer;
}

CChattingServer::~CChattingServer()
{
	if (nullptr != m_pTimer)
		delete m_pTimer;
}

void CChattingServer::LoadMapData()
{


	CObjectMgr::GetInst().m_obstacleData = {
	   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                  },
	   { 0, 0, 0, 17, 21, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0,               },
	   { 0, 0, 0, 29, 33, 0, 0, 0, 0, 13, 13, 0, 0, 0, 17, 0, 0, 0, 0, 0,             },
	   { 0, 0, 0, 29, 45, 0, 0, 25, 0, 0, 0, 0, 0, 0, 29, 0, 0, 13, 0, 0,             },
	   { 0, 0, 0, 29, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 13, 0, 0,              },
	   { 0, 0, 0, 65, 69, 0, 13, 13, 13, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0,            },
	   { 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0,                },
	   { 0, 13, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0,               },
	   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 0, 0, 0, 0, 65, 66, 66, 66, 66, 0,            },
	   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 },
	   { 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0,                },
	   { 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0,                },
	   { 0, 0, 0, 0, 13, 0, 0, 13, 13, 13, 0, 0, 13, 0, 0, 0, 13, 0, 0, 0,            },
	   { 13, 13, 13, 13, 13, 0, 0, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 0, 0, 0,       },
	   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 },
	   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 0, 0, 0, 0, 0, 0, 0, 25, 0,                },
	   { 0, 13, 0, 0, 0, 17, 0, 0, 83, 0, 0, 0, 71, 0, 0, 13, 0, 0, 25, 0,            },
	   { 0, 13, 0, 0, 0, 29, 0, 0, 95, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0,              },
	   { 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0,                },
	   { 0, 0, 0, 0, 0, 65, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0                 }
	};

}



bool CChattingServer::onRecv(int id, ExOver* over, int numData)
{
	if (numData == 0) {
		return false;
	}

	CSession* session = CObjectMgr::GetInst().GetSession(id);
	int prevSize = session->GetPrevSize(); //이전에 가지고있던 데이터 크기
	int remain_data = prevSize + numData;  //현재까지 recv된 총 패킷들의 크기의 합(잘릴 수도 있고, 모자랄 수도 있음)
	uint8_t* packet_start = over->m_buf;

	flatbuffers::uoffset_t packet_size = 0; //처리해야하는 하나의 패킷크기
	memcpy(&packet_size, packet_start, sizeof(flatbuffers::uoffset_t));  //패킷에 써있는 헤더크기를 읽는다.
	packet_size += sizeof(flatbuffers::uoffset_t); //원래 패킷데이터크기 + prefix헤더의 크기를 더한다. 그것이 recv한 패킷하나의의 총 크기이다.


	while (packet_size <= remain_data)
	{
		process_packet(id, packet_start);
		remain_data -= packet_size;  //처리했으면 나머지 데이터크기에서 빼준다.
		packet_start += packet_size; //처리했으면 처리한 패킷 사이즈만큼 전진시킨다!
		if (remain_data > 4) {
			memcpy(&packet_size, packet_start, sizeof(flatbuffers::uoffset_t)); //이부분이 문제일수도 있다.
			packet_size += sizeof(flatbuffers::uoffset_t);
		}
		else
			break;
	}
	if (remain_data > 0) {//온전한 패킷이 recv가 안되고 패킷이 잘려서 들어온 경우이다.
		session->SetPrevSize(remain_data);
		//uint8_t* newPacket_start = session->GetBuf();
		memcpy(over->m_buf, packet_start, remain_data); //recv버퍼 맨앞으로 이동시켜준다!
		//memset(packet_start, 0, sizeof(over->m_buf) - remain_data);
	}
	session->do_recv();
	return true;
}

void CChattingServer::send_login_result(int to_id, q2g_LOGIN_RESULT* packet)
{
	flatbuffers::FlatBufferBuilder builder;

	//int x = rand() % WORLD_SIZE;
	//int y = rand() % WORLD_SIZE;
	////int x = 0;
	////int y = 0;
	////if (to_id == 0) {
	////	x = 11;
	////	y = 11;
	////}
	////else
	////{
	////	x = 20;
	////	y = 20;
	////}

	int x = packet->x_pos, y = packet->y_pos, result = packet->ret, dmg = packet->dmg, max_hp = packet->max_hp;
	int lv = packet->lv, cur_exp = packet->cur_exp;
	m_sectors.InsertObject(x, y, to_id);



	auto data = fbs::CreateS2C_LOGIN_RESULT(builder, to_id, result, x, y, max_hp, dmg,
		lv, cur_exp);//내가 여기서 포인트 수정할 수 도있다.
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_LOGIN_RESULT, data.Union());
	builder.FinishSizePrefixed(root);
	CSession* session = CObjectMgr::GetInst().GetSession(to_id);
	CPlayer* player = static_cast<CPlayer*>(session);
	player->Init(x, y, max_hp, lv, cur_exp);

	//std::cout << to_id << "에게 " << builder.GetSize() << "만큼 보낸다!\n";
	session->do_send(builder.GetBufferPointer(), builder.GetSize());
}

void CChattingServer::send_message(int from_id, const fbs::Root* root)
{
	//CPlayer* session = static_cast<CPlayer*>(CObjectMgr::GetInst().GetObjectPtr(from_id));

	auto packet = root->packet_as_C2S_Chatt_Message();
	flatbuffers::FlatBufferBuilder builder;
	auto msg = builder.CreateString(packet->msg());
	auto data = fbs::CreateC2S_Chatt_Message(builder, msg);//내가 여기서 포인트 수정할 수 도있다.
	auto send_root = fbs::CreateRoot(builder, fbs::Packet_C2S_Chatt_Message, data.Union());
	builder.FinishSizePrefixed(send_root);

	for (int i = 0; i < MAX_USER; ++i)
	{
		if (i == from_id)continue;// 자기자신에게는 못보내게 하라!
		CSession* session = CObjectMgr::GetInst().GetSession(i);
		session->S_Lock();
		if (session->State() == STATE::ST_INGAME) {
			//std::cout << from_id << "가 " << i << "에게 :" << packet->msg()->Data() << "라고 보낸다!\n";
			session->do_send(builder.GetBufferPointer(), builder.GetSize());
		}
		session->S_Unlock();
	}

	//메시지는 제대로 나옴
	// 
	//std::cout << from_id << "가 " << builder.GetSize() << "만큼 보낸다!\n";
	//session->do_send(builder.GetBufferPointer(), builder.GetSize());
}

void CChattingServer::send_move_packet(int actor_id, int moveTime)
{
	auto objcet = CObjectMgr::GetInst().m_objectArr[actor_id];
	POINT pos = objcet->GetPosition();
	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateS2C_Object_Move(builder, actor_id, pos.x, pos.y, moveTime);
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_Object_Move, data.Union());
	builder.FinishSizePrefixed(root);



	for (const auto& sector : m_sectors.GetSectors(actor_id))
	{
		if (sector == nullptr)continue;
		for (int id : *sector) {
			if (false == IsNpc(id)) {
				CSession* session = CObjectMgr::GetInst().GetSession(id);
				session->S_Lock();
				if (session->State() == STATE::ST_INGAME) {
					session->do_send(builder.GetBufferPointer(), builder.GetSize());
				}
				session->S_Unlock();
			}
			else
			{

				if (false == CObjectMgr::GetInst().m_objectArr[id]->Active() && true == CObjectMgr::GetInst().IsNear(actor_id, id, playerVision))
				{

					CObjectMgr::GetInst().m_objectArr[id]->SetActive(true);
					send_add_object(actor_id, id);
					CTimer_event _event{ 1000 ,id,OTHER_WORK_OP::NPC_RANDOM_MOVE };  //1초뒤에 움직여라
					m_pTimer->add_event(_event);
				}
			}
		}
	}

}

void CChattingServer::send_move_packet(int to_id, int actor_id, int moveTime)
{
	auto objcet = CObjectMgr::GetInst().m_objectArr[actor_id];
	POINT pos = objcet->GetPosition();
	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateS2C_Object_Move(builder, actor_id, pos.x, pos.y, moveTime);
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_Object_Move, data.Union());
	builder.FinishSizePrefixed(root);

	CSession* toPaleyr = static_cast<CSession*>(CObjectMgr::GetInst().m_objectArr[to_id]);
	toPaleyr->do_send(builder.GetBufferPointer(), builder.GetSize());
}

void CChattingServer::send_add_object(int to_id, int object_id)
{
	CSession* toPaleyr = static_cast<CPlayer*>(CObjectMgr::GetInst().GetSession(to_id));
	POINT pos = CObjectMgr::GetInst().m_objectArr[object_id]->GetPosition();
	flatbuffers::FlatBufferBuilder builder;
	int maxHp = 10, curHp = 10;
	CObjectMgr::GetInst().m_objectArr[object_id]->GetHps(maxHp, curHp);
	auto data = fbs::CreateS2C_Add_Object(builder, object_id, pos.x, pos.y, maxHp, curHp);
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_Add_Object, data.Union());
	builder.FinishSizePrefixed(root);
	toPaleyr->do_send(builder.GetBufferPointer(), builder.GetSize());

}

void CChattingServer::send_remove_object(int to_id, int object_id)
{
	CSession* toPaleyr = CObjectMgr::GetInst().GetSession(to_id);
	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateS2C_Remove_Object(builder, object_id);
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_Remove_Object, data.Union());
	builder.FinishSizePrefixed(root);
	toPaleyr->do_send(builder.GetBufferPointer(), builder.GetSize());
}

void CChattingServer::send_Hitted_packet(int to_id, int object_id, int object_curHp)
{
	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateS2C_Hitted(builder, object_id, object_curHp);
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_Hitted, data.Union());
	builder.FinishSizePrefixed(root);

	CSession* session = CObjectMgr::GetInst().GetSession(to_id);
	session->do_send(builder.GetBufferPointer(), builder.GetSize());

}

void CChattingServer::send_reward_packet(CPlayer* player)
{

	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateS2C_MonsterReward(builder, player->GetExp()); //to do 이부분 나중에 몬스터 경험치로 주어야함
	auto root = fbs::CreateRoot(builder, fbs::Packet_S2C_MonsterReward, data.Union());
	builder.FinishSizePrefixed(root);

	player->do_send(builder.GetBufferPointer(), builder.GetSize());
}



void CChattingServer::initChattingServer()
{


	for (int i = 0; i < MAX_USER; ++i)
		CObjectMgr::GetInst().m_objectArr[i] = new CPlayer;
	//MapDataLoad
	LoadMapData();

	//monster 초기화
	for (int i = NPC_ID_START; i < NPC_ID_END; ++i) {
		CObjectMgr::GetInst().m_objectArr[i] = new CMonster;
		POINT pos = CObjectMgr::GetInst().m_objectArr[i]->GetPosition();
		m_sectors.InsertObject(pos.x, pos.y, i); //NPC는섹터에 전부추가
	}
	std::cout << "NPC 초기화 완료!\n";


}



void CChattingServer::send_HeatBeatCheck(CSession* sseion)
{
	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateHealth_Check(builder,0);
	auto root = fbs::CreateRoot(builder, fbs::Packet_Health_Check, data.Union());
	builder.FinishSizePrefixed(root);
	sseion->do_send(builder.GetBufferPointer(), builder.GetSize());
}

void CChattingServer::process_packet(int id, unsigned char* packet)
{
	const uint8_t* pData = (const uint8_t*)packet;
	auto root = fbs::GetSizePrefixedRoot(pData);
	auto type = root->packet_type();
	switch (type)  //스위치 케이스
	{
	case fbs::Packet_NONE:
		break;
	case fbs::Packet_C2S_LOGIN_REQ:
	{
		auto packet = root->packet_as_C2S_LOGIN_REQ();
		auto ID = packet->id();
		auto PassWord = packet->password();

		g2q_LOGIN_REQ dbPacket;
		dbPacket.id = id;
		strcpy_s(dbPacket.name, ID->c_str());
		strcpy_s(dbPacket.password, PassWord->c_str());
		dbPacket.type = G2Q_LOGIN_REQ;
		dbPacket.size = sizeof(dbPacket);
		m_pDBSession->do_dbSend(&dbPacket, dbPacket.size);
		//std::cout << packet->id()->Data() << '\n';
		//

	}
	break;
	case fbs::Packet_C2S_Enter_GAME:
	{
		CSession* session = CObjectMgr::GetInst().GetSession(id);
		session->S_Lock();
		session->SetState(STATE::ST_INGAME);
		session->S_Unlock();
		using namespace std::chrono;
		for (const auto& sector : m_sectors.GetSectors(id)) //섹터에서 데이터레이스 날 수 있지만 무시한다. 
		{
			if (sector == nullptr)continue;
			for (int other : *sector) {
				if (other == id)continue;
				if (IsNpc(other) == true) {
					if (true == CObjectMgr::GetInst().IsNear(id, other, playerVision)) { //플레이어 시야에 들어옴
						CMonster* monster = static_cast<CMonster*>(CObjectMgr::GetInst().m_objectArr[other]);
						monster->SetActive(true);
						send_add_object(id, other);
						CTimer_event _event{ 1000 ,other,OTHER_WORK_OP::NPC_RANDOM_MOVE };  //1초뒤에 움직여라
						m_pTimer->add_event(_event);
					}
					continue;
				}
				CSession* session = CObjectMgr::GetInst().GetSession(other);
				session->S_Lock();
				if (session->State() == STATE::ST_INGAME) {


					send_add_object(other, id); 		//기존플레이어들에게 새로 추가된 플레이어 추가하기


					send_add_object(id, other);			//새로들어온 플레이어에게 원래있던 플레이어 알려주기
				}
				session->S_Unlock();
			}
		}
		session->UpdateConnectTime();
		CTimer_event event = {};
		event.m_id = id;
		event.m_start_time = std::chrono::system_clock::now() + std::chrono::milliseconds(3000); //3초뒹에 검사
		event.type = OTHER_WORK_OP::Heat_Check;
		m_pTimer->add_event(event);
		send_HeatBeatCheck(session);
	}
	break;
	case fbs::Packet_C2S_Chatt_Message:
	{
		send_message(id, root);
	}
	break;


	case fbs::Packet_C2S_Move_REQ:
	{
		//std::cout << "move요청 입력왔다!\n";
		auto packet = root->packet_as_C2S_Move_REQ();
		CPlayer* player = static_cast<CPlayer*>(CObjectMgr::GetInst().GetObjectPtr(id));
		auto oldPos = player->GetPosition();
		player->MoveUpdate(packet->dir());
		auto newPos = player->GetPosition();
		m_sectors.updateSector(oldPos.x, oldPos.y, newPos.x, newPos.y, id);
		int tt = packet->move_time();
		send_move_packet(id, tt);
	}
	break;
	case fbs::Packet_Attack_Packet:
	{
		auto packet = root->packet_as_Attack_Packet();



		processBattle(id, packet->dir());


	}
	break;
	case fbs::Packet_C2S_Disconnect:
	{
		Disconnect(id);
	}
	break;
	case fbs::Packet_Health_Check:
	{
		auto session = g_ObjectMgr.GetSession(id);
		session->UpdateConnectTime();
		
	}
	break;
	default:
		std::cout << "wrong packet!\n";
		break;
	}
}

void CChattingServer::process_DBpacket(int id, unsigned char* packet)
{
	const BYTE* pData = packet;

	int type = 0;
	memcpy(&type, pData + 4, sizeof(int));  //패킷 첫번째 4바이트는 패킷의 크기이다.
	switch (type)
	{
	case Q2G_Connect_REQ:
	{

		g2q_Connect_OK p;
		p.size = sizeof(p);
		p.type = G2Q_Connect_OK;

		m_pDBSession->do_dbSend(&p, p.size);

		std::cout << "쿼리서버 연결완료!\n";
		break;
	}
	case Q2G_LOGIN_RESULT:
	{
		q2g_LOGIN_RESULT* p = reinterpret_cast<q2g_LOGIN_RESULT*>(packet);
		int toId = p->id;


		send_login_result(toId, p);
		if (p->ret == DB_RESULT::DB_LOGIN_FAIL)
			Disconnect(toId);

	}
	default:
		break;
	}
}


void CChattingServer::OtherWork(int obj_id, int op)
{
	using namespace std::chrono;
	switch (op)
	{
	case NPC_WAKE_UP:
		break;
	case NPC_RANDOM_MOVE:
	{
		auto monster = static_cast<CMonster*>(CObjectMgr::GetInst().m_objectArr[obj_id]);
		auto oldpos = monster->GetPos();
		monster->randomMove();
		auto newPos = monster->GetPos();
		m_sectors.updateSector(oldpos.x, oldpos.y, newPos.x, newPos.y, obj_id);


		for (const auto& sector : m_sectors.GetSectors(obj_id)) //섹터에서 데이터레이스 날 수 있지만 무시한다. 
		{
			if (sector == nullptr)continue;
			for (int id : *sector) {
				if (false == IsNpc(id)) {
					CSession* session = CObjectMgr::GetInst().GetSession(id);
					session->S_Lock();
					if (session->State() == STATE::ST_INGAME && CObjectMgr::GetInst().IsNear(id, obj_id, playerVision) == true) {
						send_move_packet(id, obj_id, 0);
					}
					session->S_Unlock();
				}
			}
		}

		//std::cout << "움직인다.\n";
		CTimer_event _event{ 2000 ,obj_id,OTHER_WORK_OP::NPC_RANDOM_MOVE };
		m_pTimer->add_event(_event);
		break;
	}
	case OTHER_WORK_OP::Heat_Check:
	{ 
		auto session = g_ObjectMgr.GetSession(obj_id);
		session->S_Lock();
		if (session->State() != ST_INGAME)
			break;
		session->S_Unlock();

		session->S_Lock();
		auto disTime = std::chrono::duration_cast<milliseconds>(std::chrono::system_clock::now() - session->GetConnectTime()).count();
		session->S_Unlock();
		if (disTime > 10000) //10초과할 동안 갱신 되지 않으면 접속끊기
		{
			Disconnect(obj_id);
			//std::cout << disTime << '\n';
		}
		else
		{
			CTimer_event event = {};
			event.m_id = obj_id;
			event.m_start_time = std::chrono::system_clock::now() + std::chrono::milliseconds(3000); //3초뒹에 검사
			event.type = OTHER_WORK_OP::Heat_Check;
			m_pTimer->add_event(event);
			send_HeatBeatCheck(session);
			std::cout << disTime << '\r';
		}
	}
	default:
		break;
	}

}

void CChattingServer::run()
{
	std::cout << "Server Start!\n";
	std::cout << "DB서버 연결을 하세요!\n";
	//worker(this);//싱글스레드로 테스트
	for (int i = 0; i < MAX_THREAD; ++i)
		workers.emplace_back(worker, this);


	m_pTimer->Start_timer_thread(m_hIocp);
	m_pTimer->Join_timer_thread();
	for (auto& th : workers)
		th.join();

}

void CChattingServer::Disconnect(int key)
{
	if (IsNpc(key) == true)
		return;
	CSession* session = CObjectMgr::GetInst().GetSession(key);
	//auto sessionArr = CSessionMgr::GetInst().SessionArr()

	//std::cout << key << " : 클라이언트 접속 끊었음\n";
	//To Do
	//다른 클라이언트에게 key 플레이어 접속 끈었다고 알려주기
	for (int i = 0; i < MAX_USER; ++i)
	{
		CSession* s = CObjectMgr::GetInst().GetSession(i);
		if (i == key)continue;
		s->S_Lock();
		if (ST_INGAME != s->State()) {
			s->S_Unlock();
			continue;
		}
		s->S_Unlock();
		send_remove_object(i, key);

	}
	session->S_Lock();
	closesocket(session->GetSock());
	session->SetState(ST_FREE);
	session->S_Unlock();
}

void CChattingServer::processBattle(int id, fbs::Direction dir)
{
	flatbuffers::FlatBufferBuilder builder;
	auto data = fbs::CreateAttack_Packet(builder, dir);
	auto root = fbs::CreateRoot(builder, fbs::Packet_Attack_Packet, data.Union());
	builder.FinishSizePrefixed(root);
	//toPaleyr->do_send(builder.GetBufferPointer(), builder.GetSize());
	CPlayer* attacker = static_cast<CPlayer*>(CObjectMgr::GetInst().m_objectArr[id]);
	POINT attackPos = attacker->GetPosition();
	switch (dir)
	{
	case fbs::Direction_Up:
		attackPos.y--;
		break;
	case fbs::Direction_Down:
		attackPos.y++;
		break;
	case fbs::Direction_Left:
		attackPos.x--;
		break;
	case fbs::Direction_Right:
		attackPos.x++;
		break;
	default:
		break;
	}
	// 이부분에 다른 유저들에게 해당유저가 공격했다고 알려주는 코드가 있어야함
	int attacked_id = -1;
	int curHp = -1;

	for (const auto& sector : m_sectors.GetSectors(id)) //섹터에서 데이터레이스 날 수 있지만 무시한다. 
	{
		if (sector == nullptr)continue;
		for (int other : *sector) {
			if (other == id)continue;
			if (IsNpc(other) == true) {
				CMonster* monster = static_cast<CMonster*>(CObjectMgr::GetInst().m_objectArr[other]);
				POINT monsterPos = monster->GetPos();
				if (monster->Active() == true && true == CObjectMgr::GetInst().IsCollision(monsterPos, attackPos)) {
					attacked_id = other;
					//몬스터 데미지 입히기 
					curHp = monster->Hitted(attacker->GetDmg());
					if (curHp <= 0) {
						bool isLevelUp = attacker->AddExp(40);
						if (false == isLevelUp) {
							send_reward_packet(attacker);
						}
						else {
							//levelup 업데이트 활동하기
						}
					}
				}
				continue;
			}
			else {
				CSession* session = CObjectMgr::GetInst().GetSession(other);
				session->S_Lock();
				if (session->State() == STATE::ST_INGAME) {
					//누가 공격했는지 보내기
					session->do_send(builder.GetBufferPointer(), builder.GetSize());
				}
				session->S_Unlock();
			}

		}
	}

	if (attacked_id != -1) {

		for (const auto& sector : m_sectors.GetSectors(id)) //섹터에서 데이터레이스 날 수 있지만 무시한다. 
		{
			if (sector == nullptr)continue;
			for (int other : *sector) {
				if (IsNpc(other) == true)continue;
				CSession* session = CObjectMgr::GetInst().GetSession(other);
				session->S_Lock();
				if (session->State() == STATE::ST_INGAME) {

					//충돌했으면 충돌결과 보내기
					if (attacked_id != -1) {
						send_Hitted_packet(other, attacked_id, curHp);
					}
				}
				session->S_Unlock();
			}
		}
	}


}

