#include "stdafx.h"
#include "Player.h"


CPlayer::CPlayer()
{

}
CPlayer::~CPlayer()
{

}

void CPlayer::Init(int x, int y, int max_hp, int lv, int cur_exp)
{
	SetPostion(x, y);
	m_maxHp = max_hp;
	m_curHp = max_hp;
	m_dmg = 2;
	m_lv = lv;
	m_cur_exp = cur_exp;
	m_max_exp = 100; //나중에는 레벨 비례해서 할것 같음 일단은 테스트를 위해서 이렇게 설정

}

void CPlayer::SetPostion(int x, int y)
{
	m_x_pos = x;
	m_y_pos = y;
}

void CPlayer::AddPostion(int dx, int dy)
{
	m_x_pos += dx;
	m_y_pos += dy;
}

bool CPlayer::AddExp(int exp)
{
	m_cur_exp += exp;
	if (m_cur_exp >= m_max_exp) {
		m_cur_exp = 0;
		return true;
	}
	return false;
}

int CPlayer::GetExp()
{
	return m_cur_exp;
}

int CPlayer::Hitted(int dmg)
{
	m_curHp -= dmg;
	if (m_curHp < 0)
		m_curHp = 0;
	return m_curHp;
}


bool CPlayer::MoveUpdate(fbs::Direction dir)
{
	switch (dir)
	{
	case fbs::Direction_Up:
		if (m_y_pos - 1 >= 0 && CObjectMgr::GetInst().m_obstacleData[(m_y_pos - 1) % 20][m_x_pos % 20]==0)
			AddPostion(0, -1);
		break;
	case fbs::Direction_Down:
		if (m_y_pos + 1 < WORLD_SIZE && CObjectMgr::GetInst().m_obstacleData[(m_y_pos + 1) % 20][m_x_pos % 20]==0)
			AddPostion(0, 1);
		break;
	case fbs::Direction_Left:
		if (m_x_pos - 1 >= 0 && CObjectMgr::GetInst().m_obstacleData[m_y_pos % 20][(m_x_pos - 1) % 20]==0)
			AddPostion(-1, 0);
		break;
	case fbs::Direction_Right:
		if (m_x_pos + 1 < WORLD_SIZE && CObjectMgr::GetInst().m_obstacleData[m_y_pos % 20][(m_x_pos + 1) % 20]==0)
			AddPostion(1, 0);
		break;
	default:
		break;
	}

	return true;
}

void CPlayer::Set_moveTime(std::chrono::high_resolution_clock mt)
{
	m_last_moveTime = mt;
}

std::chrono::high_resolution_clock CPlayer::GetMoveTime()
{
	return m_last_moveTime;
}


