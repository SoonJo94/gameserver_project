#include "stdafx.h"
#include "Monster.h"

CMonster::CMonster()
{
	memset(this, 0, sizeof(CMonster));
	m_maxHp = 10;
	m_curHp = m_maxHp;


	m_x_pos = rand() % WORLD_SIZE;
	m_y_pos = rand() % WORLD_SIZE;
}

CMonster::~CMonster()
{
}

void CMonster::SetPostion(int x, int y)
{
}

void CMonster::AddPostion(int dx, int dy)
{
	m_x_pos += dx;
	m_y_pos += dy;
}

void CMonster::GetPosition(int& x, int& y)
{
}

void CMonster::randomMove()
{
	static int dx[] = { 0,0,-1,1 };
	static int dy[] = { -1,1,0,0 };
	auto dir = (rand() % fbs::Direction::Direction_Right) + 1;
	int nextY = m_y_pos + dy[dir];
	int nextX = m_x_pos + dx[dir];
	int nextOY = nextY % 20;
	int nextOX = nextX % 20;
	switch (dir)
	{
	case fbs::Direction_Up: {
		if (nextY >= 0 && !CObjectMgr::GetInst().m_obstacleData[nextOY][nextOX])
			AddPostion(dx[dir], dy[dir]);
		break;
	}
	case fbs::Direction_Down:
		if (nextY < WORLD_SIZE && !CObjectMgr::GetInst().m_obstacleData[nextOY][nextOX])
			AddPostion(dx[dir], dy[dir]);
		break;
	case fbs::Direction_Left:
		if (nextX >= 0 && !CObjectMgr::GetInst().m_obstacleData[nextOY][nextOX])
			AddPostion(dx[dir], dy[dir]);
		break;
	case fbs::Direction_Right:
		if (nextX < WORLD_SIZE && !CObjectMgr::GetInst().m_obstacleData[nextOY][nextOX])
			AddPostion(dx[dir], dy[dir]);
		break;
	default:
		break;
	}
}

bool CMonster::MoveUpdate(fbs::Direction dir)
{
	return false;
}

void CMonster::init()
{
	memset(this, 0, sizeof(CMonster));

	m_x_pos = rand() % WORLD_SIZE;
	m_y_pos = rand() % WORLD_SIZE;
	m_last_move_time = std::chrono::high_resolution_clock::now();
}



int CMonster::Hitted(int dmg)
{
	m_curHp -= dmg;
	if (m_curHp < 0)
		m_curHp = 0;
	return m_curHp;
}

POINT CMonster::GetPos()
{
	return { m_x_pos,m_y_pos };
}
