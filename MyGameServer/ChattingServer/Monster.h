
#pragma once


class CMonster :public CObject
{
public:
	CMonster();
	virtual ~CMonster();

	void SetPostion(int x, int y);
	void AddPostion(int dx, int dy);
	void GetPosition(int& x, int& y);


	void randomMove();
	bool MoveUpdate(fbs::Direction dir);
	void init();

	virtual int Hitted(int dmg)final;
	POINT GetPos();
private:
	std::chrono::high_resolution_clock::time_point m_last_move_time;// 마지막으로 움직인 시각
};

