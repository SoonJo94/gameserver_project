#pragma once
#include "DB_Protocol.h"
class CTimer;
class CPlayer;
class CChattingServer:public CSERVER_IOCP
{
private:
	

	void initChattingServer();
	CSectors m_sectors;
	CTimer* m_pTimer;
	const int playerVision = 5;

	std::vector<std::vector<BYTE>> m_obstacleData;
public:
	CChattingServer();
	~CChattingServer();
	void LoadMapData();
	virtual bool onRecv(int id, ExOver* over, int numData) final;
	void send_login_result(int to_id, q2g_LOGIN_RESULT* p );
	void send_message(int from_id, const fbs::Root* root);
	void send_move_packet(int actor_id,int moveTime);
	void send_move_packet(int to_id,int actor_id, int moveTime);
	void send_add_object(int to_id, int object_id);
	void send_remove_object(int to_id, int object_id);
	void send_Hitted_packet(int to_id, int object_id, int object_curHp);
	void send_reward_packet(CPlayer* player);

	void send_HeatBeatCheck(CSession* session);
	virtual void process_packet(int id, unsigned char* packet) final;
	virtual void process_DBpacket(int id, unsigned char* packet) final;


	virtual void OtherWork(int npc_id, int op) final;
	virtual void run()final;
	virtual void Disconnect(int key) final;


public:
	void processBattle(int id, fbs::Direction dir);
};

