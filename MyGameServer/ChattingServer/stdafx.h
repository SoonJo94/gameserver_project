#pragma once


#include <ServerEngine.h>
#include "Packet_generated.h"
#ifdef _DEBUG
#pragma comment(lib,"SJ_SEVER_ENGINE_Debug.lib")
#else
#pragma comment(lib,"SJ_SEVER_ENGINE.lib")
#endif // _DEBUG