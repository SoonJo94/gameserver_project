#pragma once
class CPlayer :public CSession
{
public:
	CPlayer();
	virtual ~CPlayer();
	void Init(int x, int y, int max_hp, int lv, int cur_exp);
	void SetPostion(int x, int y);
	void AddPostion(int dx, int dy);
	bool AddExp(int exp); //levelup�ϸ� true ����
	int GetExp();
	virtual int Hitted(int dmg) final;
	//POINT GetPosition();

	bool MoveUpdate(fbs::Direction dir);

	void Set_moveTime(std::chrono::high_resolution_clock mt);
	std::chrono::high_resolution_clock GetMoveTime();

	int GetDmg() { return m_dmg; };
private:
	int m_dmg;
	int m_lv;
	int m_cur_exp;
	int m_max_exp;

	std::chrono::high_resolution_clock m_last_moveTime;
};

