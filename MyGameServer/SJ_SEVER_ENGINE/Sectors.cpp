#include "stdafx.h"
#include "Sectors.h"


//CSectors::CSectors()
//{
//
//}


CSectors::CSectors(int worldSize):m_sectorWidth(worldSize / 10)
{
	m_userSectorArray.resize(fbs::Constants::Constants_MAX_USER + fbs::Constants_MAX_NPC);
	m_sectorWidth = (worldSize / 10);
	if (m_sectorWidth < 1)
		m_sectorWidth = 1;
	m_ppSectors = new Sector * [m_sectorWidth];
	for (int i = 0; i < m_sectorWidth; ++i)
	{
		m_ppSectors[i] = new Sector[m_sectorWidth];
	}
}

CSectors::~CSectors()
{

	m_sectorLock.lock();
	if (nullptr != m_ppSectors)
	{
		for (int i = 0; i < m_sectorWidth; ++i)
		{
			delete[] m_ppSectors[i];
			m_ppSectors[i] = nullptr;
		}
		delete[] m_ppSectors;
		m_ppSectors = nullptr;
	}
	m_sectorLock.unlock();
}


void CSectors::init(int worldSize)
{
	m_sectorLock.lock();
	if (m_ppSectors != nullptr){
		m_sectorLock.unlock();
		return;
	}
		
	m_sectorWidth = (worldSize / 10);
	if (m_sectorWidth < 1)
		m_sectorWidth = 1;
	m_ppSectors = new Sector * [m_sectorWidth];
	for (int i = 0; i < m_sectorWidth; ++i)
	{
		m_ppSectors[i] = new Sector[m_sectorWidth];
	}
	m_sectorLock.unlock();
}

void CSectors::updateSector(int x, int y, int newX, int newY, int id)
{

	if (x / m_sectorWidth != newX / m_sectorWidth || y / m_sectorWidth != newY / m_sectorWidth) //이전섹터와 달라지면 update
	{
		deleteObject(id);
		InsertObject(newX, newY, id);
	}
}

void CSectors::InsertObject(int x, int y, int id)
{
	constexpr static int dx[] = { -1,0,1, -1,0,1, -1,0,1 }; //좌상 상 우상, 좌 본인 우 , 좌하, 하, 우하
	constexpr static int dy[] = { -1,-1,-1, 0, 0, 0,1,1,1 };
	int startX = x / m_sectorWidth, startY = y / m_sectorWidth;

	m_sectorLock.lock();
	m_ppSectors[startY][startX].insert(id); //해당 위치 섹터에 추가



	for (int i = 0; i < 9; ++i) {
		if (startX + dx[i] >= 0 && startY + dy[i] >= 0) {
			m_userSectorArray[id][i] = &m_ppSectors[startY+dy[i]][startX+dx[i]]; //유저가 순회할 섹터(자기 포함 인접 8방향)등록
		}
	}

	m_sectorLock.unlock();

}

void CSectors::deleteObject(int id)
{
	for (auto& sector : m_userSectorArray[id])
		sector = nullptr;

	//int startX = x / m_sectorWidth, startY = y / m_sectorWidth;

	//m_sectorLock.lock();
	//m_ppSectors[startY][startX].erase(id);

	//m_userSectorArray[id]
	//if (x > 0) {
	//	if (startX * m_sectorWidth == x)// x가 세로 섹터 경계선에 겹칠 때
	//	{
	//		m_ppSectors[startY][startX - 1].erase(id);
	//	}
	//}

	//if (y > 0) {
	//	if (startY * m_sectorWidth == y)// y가 가로 섹터 경계선에 겹칠 때
	//	{
	//		m_ppSectors[startY - 1][startX].erase(id);
	//	}
	//}

	//if (x > 0 && y > 0)
	//{
	//	if (startX * m_sectorWidth == x && startY * m_sectorWidth == y) //섹터 가로세로 경계선에 모두 겹칠 때
	//		m_ppSectors[startY - 1][startX - 1].erase(id);
	//}
	//m_sectorLock.unlock();
}

const std::array<const CSectors::Sector*,9>& CSectors::GetSectors(int id)
{

	return m_userSectorArray[id];

}
