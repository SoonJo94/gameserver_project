#include "stdafx.h"
#include <thread>
#include "Timer_event.h"

void CTimer::Start_timer_thread(HANDLE iocp)
{
	m_pTimer_thread = new std::thread(&CTimer::timer_thread, this, iocp);


}

void CTimer::Join_timer_thread()
{
	if (nullptr == m_pTimer_thread) return;
	m_pTimer_thread->join();

	delete m_pTimer_thread;
}

void CTimer::add_event(CTimer_event _event)
{

	m_lock.lock();
	m_timer_q.push(_event);
	m_lock.unlock();
}

void CTimer::add_event(const CTimer_event&& _event)
{
	m_lock.lock();
	m_timer_q.emplace(_event);
	m_lock.unlock();

}

void CTimer::timer_thread(HANDLE h_iocp_handle)
{
	using namespace std::chrono;
	while (true)
	{

		m_lock.lock();

		if (!m_timer_q.empty() && m_timer_q.top().m_start_time < std::chrono::system_clock::now())//가장 빠른시간이 현재보다 안되었음 기달려야함
		{
			CTimer_event _event = m_timer_q.top();
			m_timer_q.pop();
			m_lock.unlock();
			Over* over = new Over;
			over->m_op = IO_OP::IO_OTHER;
			over->m_other_op = _event.type;
			PostQueuedCompletionStatus(h_iocp_handle, 1, _event.m_id, &over->m_over);


		}
		else
		{
			m_lock.unlock();
			std::this_thread::sleep_for(10ms); //일정시간 기다리기
		}
	}
}

CTimer_event::CTimer_event()
{
}

CTimer_event::CTimer_event(int delay, int id, int _type) :m_start_time(std::chrono::system_clock::now() + std::chrono::milliseconds(delay)), m_id(id), type(_type)
{

}

CTimer_event::~CTimer_event()
{
}

constexpr bool CTimer_event::operator<(const CTimer_event& _left) const
{
	return m_start_time > _left.m_start_time;
}
