#pragma once


class CTimer_event
{
public:
	int m_id;
	std::chrono::system_clock::time_point	m_start_time;
	int type;

	CTimer_event();
	CTimer_event(int delay, int id, int type);
	virtual ~CTimer_event();

	constexpr bool operator <(const CTimer_event& _left) const;

};



class CTimer
{
	std::priority_queue<CTimer_event> m_timer_q;

	std::mutex m_lock;
	std::thread* m_pTimer_thread;
	void timer_thread(HANDLE h_iocp_handle);
public:

	void Start_timer_thread(HANDLE iocp);
	void Join_timer_thread();
	void add_event(CTimer_event _event);
	void add_event(const CTimer_event&& _event);

	
};

