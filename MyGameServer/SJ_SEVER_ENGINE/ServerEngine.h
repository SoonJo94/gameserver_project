#pragma once
#include "stdafx.h"
#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>

#include <functional>
#pragma comment (lib, "WS2_32.LIB")
#pragma comment (lib, "MSWSock.LIB")


#include <chrono>
//threaad 관련
#include <mutex>
#include <thread>
//컨테이너 
#include <vector>
#include <array>
#include <queue>



//내가만든 헤더
#include "Singleton.h"
#include "../SJ_SEVER_ENGINE/Protocol_generated.h"
#include "SERVER_IOCP.h"
#include "Session.h"
#include "Sectors.h"
#include "Object.h"

static const int MAX_USER = fbs::Constants::Constants_MAX_USER;
static const int MAX_NPC= fbs::Constants::Constants_MAX_NPC;
static const int NPC_ID_START = MAX_USER;
static const int NPC_ID_END = MAX_USER + MAX_NPC - 1;
static const int WORLD_SIZE = fbs::Constants::Constants_WORLD_SIZE;

//매니저
#include "ObjectMgr.h"

