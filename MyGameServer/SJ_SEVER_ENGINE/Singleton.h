#pragma once


template<class T>
class CSingleton
{
protected:
	CSingleton() {}
	virtual ~CSingleton(){}
public:
	static T& GetInst()
	{
		static T inst;
		return inst;
	}
};

