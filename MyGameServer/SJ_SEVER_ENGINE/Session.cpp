#include "stdafx.h"
#include "Object.h"
#include "Session.h"
#include <mutex>



void CSession::init()
{
	m_exOver.init();
}

void CSession::init(int id)
{
	m_exOver.init();
}

void CSession::do_send(void* data, int numBytes)
{
	m_exOver.do_send(data, numBytes);
}

void CSession::do_dbSend(void* data, int numBytes)
{
	m_exOver.do_DBsend(data, numBytes);
}

void CSession::do_dbRecv()
{
	m_exOver.do_DBrecv();
}
