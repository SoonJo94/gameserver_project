#include "stdafx.h"
#include "help.h"


//전체적으로 쓰는 함수이다.
void error_display(int err_no)
{
#ifdef _DEBUG


	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, 0);
	std::wcout <<' ' << lpMsgBuf << std::endl;
	LocalFree(lpMsgBuf);
#endif // _DEBUG
}

void error_display(const char* s)
{
	int err = WSAGetLastError();
	if (err == ERROR_IO_PENDING) {
		return;
	}
#ifdef _DEBUG

	std::cout << s << " 오류: ";
		WCHAR* lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL, err,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf, 0, 0);
		std::wcout << ' ' << lpMsgBuf << std::endl;
		LocalFree(lpMsgBuf);
#endif // _DEBUG
	

}
