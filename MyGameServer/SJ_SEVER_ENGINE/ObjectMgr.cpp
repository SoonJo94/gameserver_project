#include "stdafx.h"
#include "ObjectMgr.h"

CObjectMgr::CObjectMgr()
{
}

CObjectMgr::~CObjectMgr()
{
}

CObject* CObjectMgr::GetObjectPtr(int id)
{
    return m_objectArr[id];
}

CSession* CObjectMgr::GetSession(int id)
{

    return static_cast<CSession*>(m_objectArr[id]);
}

bool CObjectMgr::IsCollision(const POINT& p1, const POINT& p2)
{
    return p1.x==p2.x&&p1.y==p2.y;
}

bool CObjectMgr::IsNear(int a_id, int b_id,int comapreDis)
{
    POINT aPos = m_objectArr[a_id]->GetPosition();
    POINT bPos = m_objectArr[b_id]->GetPosition();

    int xDis = abs(aPos.x - bPos.x);
    int yDis = abs(aPos.y - bPos.y);

    if (comapreDis < xDis)return false;
    if (comapreDis < yDis)return false;
    return true;
}

