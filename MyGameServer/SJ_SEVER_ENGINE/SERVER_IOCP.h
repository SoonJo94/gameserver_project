#pragma once
#include "help.h"
constexpr int MAX_THREAD = 4;
constexpr int  BUFSIZE = 512;


class CSession;
class ExOver;

class CSERVER_IOCP
{
protected:

	short m_port;
	HANDLE m_hIocp;
	SOCKET m_socket;
	CSession* m_pDBSession;

	std::vector<std::thread> workers;
	//CSession* m_objectArr;


	static void worker(void* server);

public:
	CSERVER_IOCP();
	virtual ~CSERVER_IOCP();
	virtual void Init();

	void onAccept(ExOver* over);
	void onDBAccept(ExOver* over);
	virtual void run();
	bool IsNpc(int id);
	bool IsPlayer(int id);
	virtual void Disconnect(int key);// 접속 끊어버리기
	int get_new_id();
	virtual bool onRecv(int id, ExOver* over, int numData);
	virtual bool onSend(int id, ExOver* over, int numData);
	virtual void process_packet(int id, unsigned char* packet) = 0;


	bool onDB_Recv(int id, ExOver* over, int numData);
	bool onDB_Send(int id, ExOver* over, int numData);
	virtual void process_DBpacket(int id, unsigned char* packet) = 0;
	virtual void OtherWork(int id, int op);


};


