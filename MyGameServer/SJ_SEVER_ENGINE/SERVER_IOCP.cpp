#include "stdafx.h"
#include "SERVER_IOCP.h"
#include "Session.h"
#include "help.h"
//#include "DB_Protocol.h"

CSERVER_IOCP::CSERVER_IOCP()
{

}

void CSERVER_IOCP::Init()
{
	//To Do: 세션 초기화


	std::wcout.imbue(std::locale("korean"));
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	m_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED); //오버렙드 소켓만들기

	SOCKADDR_IN server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(fbs::Constants::Constants_SEVER_PORT);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (0 != bind(m_socket, reinterpret_cast<sockaddr*>(&server_addr), sizeof(server_addr)))
	{
		error_display("bind");
	}
	listen(m_socket, SOMAXCONN);

	m_hIocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0); //iocp 커널객체생성
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(m_socket), m_hIocp, 0, 0); //iocp객체에 소캣등록

	SOCKET c_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED);


	char	accept_buf[sizeof(SOCKADDR_IN) * 2 + 32 + 100];

	ExOver exover;
	memset(&exover.m_over, 0, sizeof(exover.m_over));
	exover.m_socket = c_socket;
	exover.m_op = IO_DB_ACCEPT;

	BOOL ret = AcceptEx(m_socket, c_socket, accept_buf, 0, sizeof(SOCKADDR_IN) + 16, sizeof(SOCKADDR_IN) + 16, NULL, &exover.m_over);
	if (ret == FALSE) {
		int err = WSAGetLastError();
		if (err != ERROR_IO_PENDING)
			error_display(err);
	}
	run();


	//TO DO: 연결된 클라이언트들 모두 접속 끊기


}
void CSERVER_IOCP::run()
{
	std::cout << "Server Start!\n";
	//worker(this);//싱글스레드로 테스트
	for (int i = 0; i < MAX_THREAD; ++i)
		workers.emplace_back(worker, this);

	for (auto& th : workers)
		th.join();
}

bool CSERVER_IOCP::IsNpc(int id)
{
	return (id >= NPC_ID_START) && (id <= NPC_ID_END);
}

bool CSERVER_IOCP::IsPlayer(int id)
{
	return (id >= 0) && (id < MAX_USER);
}

void CSERVER_IOCP::Disconnect(int key)
{
	CSession* session = CObjectMgr::GetInst().GetSession(key);
	closesocket(session->GetSock());
	session->S_Lock();
	session->SetState(ST_FREE);
	session->S_Unlock();
	//std::cout << key << " : 클라이언트 접속 끊었음\n";
	//To Do
	//다른 클라이언트에게 key 플레이어 접속 끈었다고 알려주기
	/*for(int i=0;i<)
	{
		s->S_Lock();
		if (ST_INGAME != s->State()) {
			s->S_Unlock();
			continue;
		}
		s->S_Unlock();
	}*/
}

int CSERVER_IOCP::get_new_id()
{

	for (int i = 0; i < MAX_USER; ++i) {
		CSession* session = CObjectMgr::GetInst().GetSession(i);
		session->S_Lock();
		if (ST_FREE == session->State()) {
			session->SetState(ST_ACCEPT);

			session->S_Unlock();
			return i;
		}
		session->S_Unlock();
	}
	std::cout << "최대 동접 초과!\n";
	return -1;
}

bool CSERVER_IOCP::onRecv(int id, ExOver* over, int numData)
{
	if (numData == 0) {
		return false;
	}
	CSession* session = CObjectMgr::GetInst().GetSession(id);
	int prevSize = session->GetPrevSize();
	int remain_data = prevSize + numData;
	//unsigned char* packet = over->m_buf; 익걸로 하면안됨 왜냐 이걸로 들어온 버퍼는 이전버퍼의 내용이 없다. 
	//WSARecv 할때 기존 세션의 버프+남아있는 바이트크기 위치에서 데이터를 받는다.
	unsigned char* packet = session->GetBuf(); //session 버퍼 읽기
	int packet_size = packet[0] + 4; //크기 접두어 사이즈만큼 더해준다.
	while (packet_size <= remain_data)
	{
		process_packet(id, packet);
		remain_data -= packet_size;
		packet += packet_size;
		if (remain_data > 0) packet_size = packet[0] + 4;
		else
			break;
	}
	if (remain_data > 0) {
		session->SetPrevSize(remain_data);
		memcpy(over->m_buf, packet, remain_data);
	}
	session->do_recv();
	return true;
}



bool CSERVER_IOCP::onSend(int id, ExOver* over, int numData)
{

	if (numData != over->m_wsaBuf.len) {
		delete over;
		return false;
	}
	delete over;
	return true;
}


bool CSERVER_IOCP::onDB_Recv(int id, ExOver* over, int numData)
{

	if (numData == 0) {
		return false;
	}
	CSession* DBsession = CObjectMgr::GetInst().GetSession(id);;
	int prevSize = DBsession->GetPrevSize(); //이전에 가지고있던 데이터 크기
	int remain_data = prevSize + numData;  //현재까지 recv된 총 패킷들의 크기의 합(잘릴 수도 있고, 모자랄 수도 있음)
	BYTE* packet_start = over->m_buf;
	int packet_size = 0; //처리해야하는 하나의 패킷크기
	memcpy(&packet_size, packet_start, sizeof(int));  //패킷에 써있는 헤더크기를 읽는다.


	while (packet_size <= remain_data)
	{
		process_DBpacket(id, packet_start);
		remain_data -= packet_size;
		packet_start += packet_size;
		if (remain_data > 4) {
			memcpy(&packet_size, packet_start, sizeof(int));
		}
		else
			break;
	}
	if (remain_data > 0) {
		DBsession->SetPrevSize(remain_data);
		//uint8_t* newPacket_start = session->GetBuf();
		memcpy(over->m_buf, packet_start, remain_data); //recv버퍼 맨앞으로 이동시켜준다
		//memset(packet_start, 0, sizeof(over->m_buf) - remain_data); //이거는 지울 수 도 있음, 쓰레기 값 안받으려고 해둠
	}
	DBsession->do_dbRecv();
	return true;
}

bool CSERVER_IOCP::onDB_Send(int id, ExOver* over, int numData)
{
	if (numData != over->m_wsaBuf.len) {
		delete over;
		return false;
	}
	delete over;
	return true;
}


void CSERVER_IOCP::OtherWork(int id, int op)
{
}


void CSERVER_IOCP::onAccept(ExOver* over)
{
	SOCKET c_socket = over->m_socket;
	int new_id = get_new_id();
	if (new_id == -1)
		return;
	CSession* s = CObjectMgr::GetInst().GetSession(new_id);
	s->init();
	s->SetId(new_id);
	s->SetSock(c_socket);
	s->init();
	//IOCP객체에 등록
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), m_hIocp, new_id, 0);
	//std::cout << " accept 완료\n";
	s->do_recv();

	memset(&over->m_op, 0, sizeof(over->m_op));
	c_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED);

	over->m_socket = c_socket;
	over->m_op = IO_ACCEPT;
	AcceptEx(m_socket, c_socket, over->m_buf + 8, 0, sizeof(SOCKADDR_IN) + 16,  //64bit이므로 8바이트 해야함
		sizeof(SOCKADDR_IN) + 16, NULL, &over->m_over);

}

void CSERVER_IOCP::onDBAccept(ExOver* over)
{
	SOCKET c_socket = over->m_socket;
	int new_id = get_new_id();
	if (new_id == -1)
		return;
	CSession* s = CObjectMgr::GetInst().GetSession(new_id);
	s->init();
	s->SetId(new_id);
	s->SetSock(c_socket);
	s->init();
	//IOCP객체에 등록
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(c_socket), m_hIocp, new_id, 0);
	//std::cout << " accept 완료\n";
	m_pDBSession = s;
	s->do_dbRecv();

	memset(&over->m_op, 0, sizeof(over->m_op));
	c_socket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, 0, 0, WSA_FLAG_OVERLAPPED);

	over->m_socket = c_socket;
	over->m_op = IO_ACCEPT;
	AcceptEx(m_socket, c_socket, over->m_buf + 8, 0, sizeof(SOCKADDR_IN) + 16,  //64bit이므로 8바이트 해야함
		sizeof(SOCKADDR_IN) + 16, NULL, &over->m_over);
}


CSERVER_IOCP::~CSERVER_IOCP()
{
	closesocket(m_socket);
	WSACleanup();
}


void CSERVER_IOCP::worker(void* server)
{
	CSERVER_IOCP* iocp = static_cast<CSERVER_IOCP*>(server);

	while (true)
	{
		DWORD numBytes;
		LONG64 key;
		//memset(&key, 0, sizeof(key));
		WSAOVERLAPPED* over;
		BOOL ret = GetQueuedCompletionStatus(iocp->m_hIocp, &numBytes, (PULONG_PTR)&key, &over, INFINITE);

		int id = static_cast<int>(key);
		Over* exOver = reinterpret_cast<Over*>(over);
		if (ret == FALSE)
		{
			int err = WSAGetLastError();
			if (err != ERROR_IO_PENDING) {

#ifdef _DEBUG
				std::cout << err << " GQCS err: ";

				error_display(err);
#endif
				iocp->Disconnect(id);
				if (exOver->m_op == IO_OP::IO_WRITE)
					delete exOver;
				continue;
			}
		}


		switch (exOver->m_op)
		{
		case IO_READ: {
			if (iocp->onRecv(id, static_cast<ExOver*>(exOver), numBytes) == false)
				iocp->Disconnect(id);
		}
					break;
		case IO_WRITE:
			if (iocp->onSend(id, static_cast<ExOver*>(exOver), numBytes) == false)
				iocp->Disconnect(id);
			break;
		case IO_ACCEPT:
			//std::cout << "Accept ok\n";
			iocp->onAccept(static_cast<ExOver*>(exOver));
			break;

		case IO_DB_RECV:
			iocp->onDB_Recv(id, static_cast<ExOver*>(exOver), numBytes);

			break;
		case IO_DB_SEND:
			iocp->onDB_Send(id, static_cast<ExOver*>(exOver), numBytes);
			break;
		case IO_DB_ACCEPT:
			iocp->onDBAccept(static_cast<ExOver*> (exOver));
			break;
		case IO_OTHER:
			iocp->OtherWork(id, exOver->m_other_op);
			break;
		}



	}
}
