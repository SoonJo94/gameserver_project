#pragma once
#include <stack>
#include <unordered_map>
class CLock
{
protected:
	
public:
	virtual void lock() = 0;
	virtual void unlock() = 0;
};


class CMutexLock :public CLock
{
private:
	std::string m_lockName;
	std::mutex m_lock;

public:
	CMutexLock() = delete;
	CMutexLock(const std::string& name) :m_lockName(name) {}
	virtual void lock() final;
	virtual void unlock() final;
};


#ifdef _DEBUG
extern thread_local std::stack<int> tls_lockStack;
class CDeadlockFinder
{
private:
	std::unordered_map <std::string, int> m_lockTable;//lockID를 받아서 쉬운 ID로 변환(lockTable의 수만큼)
	std::unordered_map < int, std::string> m_lockTableReverse;//쉬운ID를 lockID로
	std::unordered_map <int, std::set<int>> m_lockList;//lockID에 포함된 list

	std::mutex m_lock;

	void CheckDeadLock();
	void DFS(int cur);

private:
	std::vector<int> m_discover;//사이클을 확인하기위해
	std::vector<int> m_parents;
	int m_discoverCnt;
	std::vector<bool> m_finished; //탐색끝
public:
	void PushLock(const std::string& lockID);
	void PopLock(const std::string& lockID);

};
extern CDeadlockFinder g_deadLockFinder;
#endif // _DEBUG