#pragma once
class CObjectMgr :public CSingleton<CObjectMgr>
{

	//const int  MAX_USER = 2000;
	//const int  MAX_NPC = 0;
	//const int NPC_ID_START = MAX_USER;
	//const int NPC_ID_END = MAX_USER + MAX_NPC - 1;



public:

	CObjectMgr();
	~CObjectMgr();

	std::vector<std::vector<BYTE>> m_obstacleData;
	std::array<CObject*, fbs::Constants::Constants_MAX_USER + fbs::Constants::Constants_MAX_NPC> m_objectArr;
	std::array<CObject*, fbs::Constants::Constants_MAX_USER + fbs::Constants::Constants_MAX_NPC>& ObjectArr() { return m_objectArr; };

	CObject* GetObjectPtr(int id);
	CSession* GetSession(int id);

	//변경되었는데 왜 복사안되었지?
	bool IsCollision(const POINT& p1, const POINT& p2);
	bool IsNear(int a_id, int b_id, int comapreDis);

};


#define g_ObjectMgr		CObjectMgr::GetInst()

