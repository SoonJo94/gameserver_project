local Node={}
function Node:new()
    local o=setmetatable({},self)
    self.__index=self
    o.children={}
    return o
end

function Node:add_child(child)
    table.insert(self.children,child)
end

function Node:add_children(children)
    for k,v in pairs(children) do
        table.insert( self.children,v )
    end
end

function Node:Print_children()
    for k,v in pairs(self.children) do
        print(v)
    end
end

local test=Node:new()

-- test:add_child(100)
-- test:Print_children()
-- test:add_children({1,2,3,4})
-- test:Print_children()

-- behavior_tree.lua

--chatGPT에서 질의한 행동트리

local NodeType = {
  Action = 1,
  Decision = 2,
}

local Node = {}
function Node:new(nodeType)
  local node = {nodeType = nodeType}
  setmetatable(node, self)
  self.__index = self
  return node
end

local ActionNode = Node:new(NodeType.Action)
function ActionNode:new(actionFunc)
  local node = Node.new(self, NodeType.Action)
  node.actionFunc = actionFunc
  return node
end

function ActionNode:execute(context)
  context.movement = self.actionFunc(context)
end

local DecisionNode = Node:new(NodeType.Decision)
function DecisionNode:new(conditionFunc)
  local node = Node.new(self, NodeType.Decision)
  node.conditionFunc = conditionFunc
  node.trueChild = nil
  node.falseChild = nil
  return node
end

function DecisionNode:execute(context)
  if self.conditionFunc(context) then
    self.trueChild:execute(context)
  else
    self.falseChild:execute(context)
  end
end

function behaviorTree(context)
  local root = DecisionNode:new(function(context) return context.health > 50 end)
  root.trueChild = ActionNode:new(function(context) return "attack" end)
  root.falseChild = DecisionNode:new(function(context) return context.health > 25 end)
  root.falseChild.trueChild = ActionNode:new(function(context) return "retreat" end)
  root.falseChild.falseChild = ActionNode:new(function(context) return "flee" end)
  root:execute(context)
end

-- monster.lua

-- Load the behavior tree script
local behaviorTree = require("behavior_tree")

-- Create a context table for the behavior tree
local context = {health = 40}

-- Execute the behavior tree with the context table
behaviorTree(context)

-- Print the result of the behavior tree
print("Movement: " .. context.movement)  -- Outputs: "flee"