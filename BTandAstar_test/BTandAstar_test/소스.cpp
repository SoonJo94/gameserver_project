#include <iostream>
#include <queue>
#include <unordered_map>
using namespace std;


extern "C"
{
#include "luaLib/include/lauxlib.h"
#include "luaLib/include/lua.h"
#include "luaLib/include/lua.hpp"
#include "luaLib/include/luaconf.h"
#include "luaLib/include/lualib.h"
}
constexpr int N = 5;
constexpr char WALL = '#';
constexpr char MONSTER = 'M';
constexpr char TARGET = 'T';
constexpr int WIDTH = 20;
//CLI기반 테스트
//장애물 적절히 섞인 맵만들기
//Monster와 타겟 적절한 위치에 배치
//Monster는 BT로 움짐직임 배회하고있다가 Target이 일정위치안에 있으면 Target을 향해 움직인다.
//매번 Target은 움직이거나 움직이지 않게 입력을 받기
// BT는 lua에서 길찾기는 C에서 구현한다. 

char MAP[20][20] = {
   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                  },
   { 0, 0, 0, 17, 21, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0,               },
   { 0, 0, 0, 29, 33, 0, 0, 0, 0, 13, 13, 0, 0, 0, 17, 0, 0, 0, 0, 0,             },
   { 0, 0, 0, 29, 45, 0, 0, 25, 0, 0, 0, 0, 0, 0, 29, 0, 0, 13, 0, 0,             },
   { 0, 0, 0, 29, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 13, 0, 0,              },
   { 0, 0, 0, 65, 69, 0, 13, 13, 13, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0,            },
   { 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0,                },
   { 0, 13, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0,               },
   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 0, 0, 0, 0, 65, 66, 66, 66, 66, 0,            },
   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 },
   { 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0,                },
   { 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 0, 0,                },
   { 0, 0, 0, 0, 13, 0, 0, 13, 13, 13, 0, 0, 13, 0, 0, 0, 13, 0, 0, 0,            },
   { 13, 13, 13, 13, 13, 0, 0, 13, 0, 0, 0, 0, 13, 13, 13, 13, 13, 0, 0, 0,       },
   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 },
   { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 95, 0, 0, 0, 0, 0, 0, 0, 25, 0,                },
   { 0, 13, 0, 0, 0, 17, 0, 0, 83, 0, 0, 0, 71, 0, 0, 13, 0, 0, 25, 0,            },
   { 0, 13, 0, 0, 0, 29, 0, 0, 95, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0,              },
   { 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0,                },
   { 0, 0, 0, 0, 0, 65, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0                 }
};

void PrintMAP();
void InitMAP()
{
	for (auto& item : MAP) {
		for (char& c : item) {
			if (c != 0)
				c = WALL;
			else
				c = '.';
		}
	}
}

struct POS
{
	int x;
	int y;

	bool operator==(const POS& rhs) const
	{
		return x == rhs.x && y == rhs.y;
	}

	bool operator!=(const POS& rhs) const
	{
		return x != rhs.x || y != rhs.y;
	}

};

class Object
{
public:
	POS pos;
	POS parents;
	int f = 123456;

	bool operator>(const Object& rhs)const
	{
		return f > rhs.f;
	}
};

Object g_monster{ 0,0,-1,-1 };
Object g_target{ 13,12,-1,-1 };
//Object g_target{ 4,7,-1,-1 };
void Astar(Object& m, Object& t);
void Input()
{
	cout << "monster 시작위치 입력(x,y) : "; cin >> g_monster.pos.x >> g_monster.pos.y;
	cout << "target 시작위치 입력(x,y) : "; cin >> g_target.pos.x >> g_target.pos.y;
}
int main()
{
	// 콘솔화면에서 잘보이기위해 맵 전처리
	InitMAP();
	while (true)
	{

		system("cls");


		//Input();
		MAP[g_monster.pos.y][g_monster.pos.x] = MONSTER;
		MAP[g_target.pos.y][g_target.pos.x] = TARGET;
		PrintMAP();
		Astar(g_monster, g_target);
		int answer = 0;
		cout << "retry? yes=0, no=-1 : "; cin >> answer;
		if (answer == -1)break;
	}

}



void PrintMAP()
{
	int row = 0;
	for (const auto& item : MAP) {
		for (char c : item) {
			cout << c << ' ';
		}
		cout << ' ' << row % 10 << '\n';
		row++;
	}
	for (int i = 0; i < row; ++i) {
		cout << i % 10 << ' ';
	}
	printf("\nMonster position (X: %d, Y: %d) \nTarget position (X: %d, Y: %d)\n", g_monster.pos.x, g_monster.pos.y, g_target.pos.x, g_target.pos.y);
}

struct pair_hash {
	template <class T1, class T2>
	std::size_t operator () (const std::pair<T1, T2>& p) const {
		auto h1 = std::hash<T1>{}(p.first);
		auto h2 = std::hash<T2>{}(p.second);

		return h1 ^ h2;
	}
};
void Astar(Object& m, Object& t)
{
	constexpr static int dx[] = { 0,0,-1,1 };
	constexpr static int dy[] = { -1,1,0,0 };
	priority_queue<Object, vector<Object>, greater<Object>> pq;

	unordered_map<pair<int, int>, Object, pair_hash> closeList;
	//int h = abs(m.pos.x - t.pos.x) + abs(m.pos.y - t.pos.y);
	m.f = 0;
	m.parents = { -1,-1 };
	pq.push(m);
	closeList[{m.pos.x, m.pos.y}] = m;


	while (pq.empty() == false)
	{
		auto Node = pq.top(); pq.pop();
		if (Node.pos == t.pos) {
			t.parents = Node.parents;
			break;
		}
		if (Node.f > closeList[{Node.pos.x, Node.pos.y}].f)continue;
		for (int i = 0; i < 4; ++i)
		{
			POS nextPos = { Node.pos.x + dx[i],Node.pos.y + dy[i] };
			if (nextPos.x < 0 || nextPos.x >= WIDTH || nextPos.y < 0 || nextPos.y >= WIDTH || MAP[nextPos.y][nextPos.x] == WALL) continue;
			int DX = abs(nextPos.x - t.pos.x), DY = abs(nextPos.y - t.pos.y);
			int h = DX * DX + DY * DY;
			int nextF = h + 1;//현재노드와 연결된 거리는 전부 1이다.
			if (closeList.count({ nextPos.x,nextPos.y }) == 0 || closeList[{nextPos.x, nextPos.y}].f > nextF) {
				closeList[{nextPos.x, nextPos.y}] = { nextPos,Node.pos,nextF };
				pq.push({ nextPos,Node.pos,nextF });

			}

		}
	}

	auto& nextPos = closeList[{t.pos.x, t.pos.y}].parents;
	int cost = 0;
	while (nextPos != m.pos) {
		MAP[nextPos.y][nextPos.x] = MONSTER;
		nextPos = closeList[{nextPos.x, nextPos.y}].parents;
		cost++;
	}

	PrintMAP();
	cout << "이동 비용 : " << cost << endl;
}