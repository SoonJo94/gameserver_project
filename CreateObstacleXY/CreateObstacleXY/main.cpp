#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <unordered_map>
#include <bitset>
using namespace std;

struct Point
{
	short x, y;

	//friend ostream& operator<<(ostream& os, const Point& point);
	//friend istream& operator>>(istream& os, Point& point);
	bool operator == (const Point& right) const
	{
		return x == right.x && y == right.x;
	}

};

//
//template<>
//struct hash<Point> {
//	size_t operator()(const Point& t)const
//	{
//		hash<short> h;
//
//		return h(t.x) ^ h(t.y);
//	}
//};


//istream& operator>>(istream& is, Point& point)
//{
//	is >> point.x >> point.y;
//	return is;
//}
//ostream& operator<<(ostream& os, const Point& point)
//{
//	os << point.x << ' ' << point.y << '\n';
//	return os;
//}

int combineShort(short x, short y)
{

	int result = x;
	//cout << bitset<32>(result) << endl;
	result = result << 16;
	//cout << bitset<32>(result) << endl;
	result |= y;
	//cout << bitset<32>(result) << endl;
	return result;
}
int main()
{

	short  data[20][20] = {
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 1380, 1380, 0, 0, 0, 0, 0            },
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 1380, 0, 0                 }		,
		{0, 0, 0, 0, 1380, 0, 0, 0, 1380, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 }		,
		{0, 0, 1794, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0,                 }		,
		{0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 1380, 0, 0, 0, 1380, 0,              }		,
		{0, 0, 1380, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0,                 }		,
		{0, 0, 0, 0, 1380, 0, 0, 0, 0, 1380, 1380, 0, 0, 0, 0, 0, 0, 0, 1380, 0,              }		,
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                          }		,
		{0, 0, 1380, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                    }		,
		{0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 1380, 1380, 0, 0, 0, 1380, 0,              }		,
		{0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                       }		,
		{0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 1380, 0, 0, 0,                 }		,
		{0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 1380, 0, 0, 0,                 }		,
		{0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0,                    }		,
		{0, 1380, 0, 0, 0, 0, 0, 1380, 1380, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0,              }		,
		{0, 1380, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 1380, 0, 0,              }		,
		{0, 0, 0, 1380, 1380, 1380, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0, 0,           }		,
		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0,                       }		,
		{0, 0, 0, 0, 1380, 1380, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1380, 0,              }		,
		{0, 0, 0, 0, 1380, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0                        }
	};


	vector<Point> v;
	for (short i = 0; i < 20; ++i)
	{
		for (short j = 0; j < 20; ++j) {
			if (data[i][j] != 0)
			{
				v.push_back({ j, i });
			}
			cout << data[i][j] << ' ';
		}
		cout << endl;
	}


	vector<int>combineVector;
	for (auto& d : v)
	{


		for (short i = 0; i < 100; ++i) {

			int x = 0, y = 0;
			x = d.x + (i * 20); y = d.y;
			combineVector.push_back(combineShort((d.x + (i * 20)), d.y));

			x = d.x;  y =d.y + (i * 20);
			combineVector.push_back(combineShort(d.x, d.y + (i * 20)));

			x = d.x + (i * 20);y= d.y + (i * 20);
			combineVector.push_back(combineShort(d.x + (i * 20), d.y + (i * 20)));
			//unMap.insert({ combineShort((d.x + (i * 20)),d.y) ,true }); //우측으로 커지기
			//unMap.insert({ combineShort(d.x,d.y + (i * 20)),true });//하단으로 커지기
			//unMap.insert({ combineShort(d.x + (i * 20), d.y + (i * 20)),true });//대각선으로 커지기 
		}
	}
	ofstream out("obstacleInfo2.bin", ios::binary);

	if (!out)
	{
		cout << "파일 쓰기 실패!\n";
	}


	copy(combineVector.begin(), combineVector.end(), std::ostream_iterator<int>{ out, " " });

	out.close();


	ifstream in("obstacleInfo2.bin", ios::binary);
	if (!in)
	{
		cout << "파일 읽기 실패!\n";
	}

	vector<int> readVector = { istream_iterator<int>(in),istream_iterator<int>() };

	cout << readVector.size() << endl;
	cout << readVector[0] << endl;
	short x = readVector[0] >> 16;
	short y = readVector[0] & 0x0000ffff;

	cout << combineShort(x, y) << endl;
	unordered_map<int, bool> unMap;
	for (const auto& rv : readVector)
	{
		unMap.insert({ rv,true });
	}

	cout << unMap.size();
}