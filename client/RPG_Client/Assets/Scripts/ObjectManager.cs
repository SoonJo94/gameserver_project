using System.Collections;
using System.Collections.Generic;
using UnityEngine;

readonly public struct PlayerInitInfo
{
    public PlayerInitInfo(int id, int x, int y, int max_hp, int dmg, int lv, int cur_exp)
    {
        this.ID = id;
        this.X = x;
        this.Y = y;
        this.MAX_HP = max_hp;
        this.DMG = dmg;
        this.LV = lv;
        this.CUR_EXP = cur_exp;
    }

    public int ID { get; }
    public int X { get; }
    public int Y { get; }
    public int DMG { get; }
    public int MAX_HP { get; }
    public int LV { get; }
    public int CUR_EXP { get; }
}

public class ObjectManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject monsterPrefab;
    public GameObject gameGridPrefab;

    public GameObject UImgrObject;
    public InGameUI UImgr;
    public Dictionary<int, GameObject> objects;

    const int MAX_USER = ConstValues.MAX_USER;
    const int MAX_NPC = ConstValues.MAX_NPC;
    public static PlayerAction player; //외부에서도 사용가능
    int playerId;

    const int monsterNums = 100;
    Queue<GameObject> monsters;
    const int visionRange = 20;//시야
    void Awake()
    {
        NetworkMgr.instance.objManager = this;
        //objects = new GameObject[MAX_USER+MAX_NPC];
        objects = new Dictionary<int, GameObject>();

        UImgr = UImgrObject.GetComponent<InGameUI>();
        Init();
    }

    bool IsPlayer(int id)
    {
        return id < MAX_USER;
    }

    void Add_Player()
    {
        var playerinfo = NetworkMgr.instance.playerInitInfo;
        playerId = NetworkMgr.instance.playerInitInfo.ID;
        objects[playerId] = Instantiate(playerPrefab);
        player = objects[playerId].GetComponent<PlayerAction>(); //이부분을 씬바뀌고 적용해야한다. 어떻게? 
        objects[playerId].SetActive(true);
        player.transform.position = new Vector3(NetworkMgr.instance.playerInitInfo.X, NetworkMgr.instance.playerInitInfo.Y, 1);
        player.SetIsPlayerTrue();
        player.SetMaxHp(playerinfo.MAX_HP);
        player.SetDmg(playerinfo.DMG);
        player.cur_exp = playerinfo.CUR_EXP;
        player.lv = playerinfo.LV;
        UImgr.maxhp = playerinfo.MAX_HP;
        UImgr.curhp = playerinfo.MAX_HP;
        UImgr.curExp = playerinfo.CUR_EXP;
        UImgr.lv = playerinfo.LV;


    }

    public void SetPlayerExp(int exp)
    {
        player.cur_exp = exp;
    }
    private void Update()
    {
        var playerinfo = NetworkMgr.instance.playerInitInfo;
        UImgr.maxhp = player.max_hp;
        UImgr.curhp = player.cur_hp;
        UImgr.curExp = player.cur_exp;
        UImgr.lv = player.lv;


        foreach (var id in objects.Keys)
        {
            if (IsPlayer(id) == true) continue;
            int disX = (int)(objects[id].transform.position.x - player.transform.position.x);
            int disY = (int)(objects[id].transform.position.y - player.transform.position.y);
            disX = disX * disX;
            disY = disY * disY;
            if (disX+disY>visionRange*visionRange) //플레이어 시야 밖이면
            {
                objects[id].SetActive(false);
                monsters.Enqueue(objects[id]);
            }
        }
  
    }

    void Init()
    {

        monsters = new Queue<GameObject>();
        for(int i = 0; i < monsterNums; ++i)
        {
            GameObject monster = Instantiate(monsterPrefab);
            monster.SetActive(false);
            monsters.Enqueue(monster);

        }

        //플레이어 정보 초기화
        Add_Player();
        gameGridPrefab = Instantiate(gameGridPrefab);
        gameGridPrefab.GetComponent<Map>().Init(objects[playerId]);

        CameraScript.instance.inGame = true;
        CameraScript.instance.player = objects[playerId];

        //나중에 NPC도 추가해야함

        NetworkMgr.instance.send_Enter_game(); //화면바뀐거 보낸다.
    }

    //public GameObject MakeObj(string type)
    //{

    //}


    public void ObjectMove(int id, int x, int y)
    {

        //if (false == objects.ContainsKey(id))
        //{
        //    Add_Object(id, x, y);
        //}
        if (id < MAX_USER)
        {
            objects[id].SetActive(true);
            PlayerAction obj = objects[id].GetComponent<PlayerAction>();
            obj.moveStart(x, y);
        }
        else
        {
            // objects[id].SetActive(true);
            //Monster obj = objects[id].GetComponent<Monster>();

            if (objects.ContainsKey(id) == false)
            { //이미 추가된 객체면 패스~
                LoadMonster(id);
            }
            objects[id].transform.position = new Vector3(x, y, 0.0f);

       
            //obj.moveStart(x, y);
        }
    }
    public void HittedObj(int id, int curHp)
    {
        if (id < MAX_USER)
        {
            PlayerAction obj = objects[id].GetComponent<PlayerAction>();
            obj.Hitted(curHp);

        }
        else
        {
            Monster obj = objects[id].GetComponent<Monster>();
            obj.Hitted(curHp);
        }
    }
    public void RemoveObject(int id)
    {
        objects[id].SetActive(false);
    }

    void LoadMonster(int id)
    {
        objects[id] = monsters.Dequeue();
        objects[id].SetActive(true);
    }
    public void Add_Object(int id, int x, int y, int maxHp, int curHp)
    {

        if (objects.ContainsKey(id) == true)
        { //이미 추가된 객체면 패스~
            //objects[id].SetActive(true);
            //objects[id].transform.position = new Vector3(x, y, 1f);
            return;
        }


        if (IsPlayer(id))
        {
            objects[id] = Instantiate(playerPrefab);
            objects[id].SetActive(true);
            objects[id].transform.position = new Vector3(x, y, 1f);
            PlayerAction player = objects[id].GetComponent<PlayerAction>();
            player.SetHps(maxHp, curHp);

        }
        else
        {

            LoadMonster(id);
            objects[id].transform.position = new Vector3(x, y, 1f);
            Monster monster = objects[id].GetComponent<Monster>();
            monster.SetHps(maxHp, curHp);
            monster.monsterName.text = "modnster" + id.ToString();
        }


    }
}
