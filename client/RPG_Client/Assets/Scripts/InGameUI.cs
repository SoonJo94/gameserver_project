using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    // Start is called before the first frame update

    public Slider playerHp;
    public Slider playerExp;
    public Text hpText;
    public Text expText;
    public Text lvText;
    public int temp;
    public int maxhp { get; set; }
    public int curhp { get; set; }
    public int curExp { get; set; }
    public int lv { get; set; }
    int maxExp;
    void Start()
    {
        maxhp = 1;
        curhp = 1;
        lv = 1;
        curExp = 1;
        maxExp = 100;
    }

    // Update is called once per frame
    void Update()
    {
        temp = maxhp;
        playerHp.maxValue = maxhp;
        playerExp.maxValue = maxExp;
        playerHp.value = curhp;
        playerExp.value = curExp;
        lv = lv;
        hpText.text = curhp.ToString() + "/" + maxhp.ToString();
        expText.text = curExp.ToString() + "/" + maxExp.ToString();
        lvText.text = lv.ToString();
       
    }
}
