using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Monster : MonoBehaviour
{
    public float maxSpeed;
    public int maxHp=1;
    private int curHp;
    public Animator anim;

    public GameObject hpBar;
    public Image hpFilled;
    public TMP_Text monsterName;
    //float h;
    //float v;
    float moveTime = 0.0f;
    bool isMovePosible = true;
    bool moveflag = false;
    enum Direction : int { Down = 0, Up, Left, Right };
    int dir;
    Vector3 StartPos;
    Vector3 EndPos;


    void Awake()
    {
        //NetworkMgr.instance.player = this;
        dir = 0;
        curHp = maxHp;
        hpFilled.fillAmount = 1f;


    }

    void Update()
    {



        //MoveUpdate();

        //anim.SetInteger("Dir", dir);
       // anim.SetBool("isMove", moveflag);

    }
    void FixedUpdate()
    {
        //Vector2 moveVec = isHorizonMove ? new Vector2(h, 0) : new Vector2(0, v);
        //rigid.velocity = moveVec * 10.0f;

    }
    public void SetHps(int max, int cur)
    {
        maxHp = max;
        curHp = cur;
    }
    public void Hitted(int hp)
    {
        curHp = hp;
        hpFilled.fillAmount = (float)curHp / maxHp;
    }
    public void moveStart(int x, int y)
    {
     
        var pos = transform.position;
        if (x < pos.x)//�������� �̵�
        {
            dir = (int)Direction.Left;
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (x > pos.x)
        {
            dir = (int)Direction.Right;
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (y > pos.y)
            dir = (int)Direction.Up;
        else if (y < pos.y)
            dir = (int)Direction.Down;

        moveflag = true;
       // anim.SetTrigger("moveTrigger");
        moveTime = 0.0f;
        StartPos = pos;
        EndPos = new Vector3(x, y, 1f);
    }
    void MoveUpdate()
    {
        if (true == moveflag)
        {
            moveTime += Time.deltaTime;
            if (moveTime > 1.0f)
            {
                moveTime = 1.0f;
                moveflag = false;
                //anim.ResetTrigger("moveTrigger");


            }
            transform.position = ((1.0f - moveTime) * StartPos) + (moveTime * EndPos);
        }
        else
        {
            float x = Mathf.Round(transform.position.x);
            float y = Mathf.Round(transform.position.y);
            transform.position = new Vector3(x, y, 0.0f);
        }
    }
}
