using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ConstValues
{
    public static readonly float X_SIZE = 1f;
    public static readonly float Y_SIZE = 1f;
    public static readonly  float moveDelay = 1.0f; //1�� ������

   public const int MAX_USER = (int)fbs.Constants.MAX_USER;
   public const int MAX_NPC = (int)fbs.Constants.MAX_NPC;
   public const int NPC_ID_START = MAX_USER;
   public const int NPC_ID_END = MAX_USER + MAX_NPC - 1;
    public const int SERVER_PORT = (int)fbs.Constants.SEVER_PORT;

}
