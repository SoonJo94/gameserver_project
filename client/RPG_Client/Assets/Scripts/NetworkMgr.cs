using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System;
using UnityEngine.SceneManagement;
using FlatBuffers;
public class NetworkMgr : MonoBehaviour
{
    // Start is called before the first frame update
    public static NetworkMgr instance = null;
    public ChattingManager chattingManager;
    public ObjectManager objManager;
    private static Socket socket;
    private static bool islogin;
    private static byte[] recvBuf;
    private static int remain_data = 0;


    public PlayerInitInfo playerInitInfo; //읽기전용


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else if (instance != this)
            Destroy(this.gameObject);


    }

    void ProcessPacket(ref ByteBuffer flatBuf)
    {


        var root = fbs.Root.GetRootAsRoot(flatBuf);
        var type = root.PacketType;
        switch (type)
        {
            case fbs.Packet.C2S_Chatt_Message:
                {
                    var packet = root.PacketAsC2S_Chatt_Message();

                    chattingManager.SendMessageToChat(packet.Msg, Message.MessageType.playerMessage, false);
                    Debug.Log("왔다" + packet.Msg);

                    break;

                }
            case fbs.Packet.S2C_Object_Move:
                {

                    var packet = root.PacketAsS2C_Object_Move();

                    objManager.ObjectMove(packet.Id, packet.XPos, -packet.YPos);  //서버랑 다르게 Y 반대 방향으로
                }
                break;
            case fbs.Packet.S2C_Add_Object:
                {
                    var packet = root.PacketAsS2C_Add_Object();
                    objManager.Add_Object(packet.Id, packet.XPos, -packet.YPos, packet.MaxHp, packet.CurHp);

                }
                break;
            case fbs.Packet.S2C_Remove_Object:
                {
                    var packet = root.PacketAsS2C_Remove_Object();
                    objManager.RemoveObject(packet.Id);

                }
                break;

            case fbs.Packet.S2C_Hitted:
                {
                    var packet = root.PacketAsS2C_Hitted();
                    objManager.HittedObj(packet.Id, packet.CurHp);
                }
                break;

            case fbs.Packet.S2C_MonsterReward:
                {
                    var packet = root.PacketAsS2C_MonsterReward();
                    objManager.SetPlayerExp(packet.CurExp);
                }
                break;
            case fbs.Packet.Health_Check:
                {
                    send_HeatBeatCheck();
                }
                break;
            default:
                Debug.Log("알수 없는 패킷 도착!\n");
                break;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (islogin == false)
            return;
        // Debug.Log(socket);
        try
        {
            int recvSize = socket.Receive(recvBuf, remain_data, recvBuf.Length - remain_data, SocketFlags.None);
            remain_data += recvSize; //기존 남은 데이터에 이번에 들어온데이터크기를 추가한다.
            int packet_size = BitConverter.ToInt32(recvBuf, 0);// 크기를 받는다.
            int recvBuf_offset = 0;
            packet_size += sizeof(int);
            if (remain_data > 0)
                while (packet_size <= remain_data)
                {
                    var flatBuf = new FlatBuffers.ByteBuffer(recvBuf, recvBuf_offset + 4);
                    ProcessPacket(ref flatBuf);
                    remain_data -= packet_size;
                    recvBuf_offset += packet_size;
                    if (remain_data >= 4)
                    {
                        packet_size = BitConverter.ToInt32(recvBuf, recvBuf_offset);// 크기를 받는다.
                        packet_size += sizeof(int);

                    }
                    else
                        break;
                }
            if (remain_data > 0)
            {
                //버퍼 앞으로 복사해야된다.
                Buffer.BlockCopy(recvBuf, recvBuf_offset, recvBuf, 0, remain_data);
            }
        }
        catch (SocketException e)
        {
            if (e.ErrorCode == 10035)
            {
                //무시
            }
            else
            {
                Console.WriteLine("{0} : {1}", e.ErrorCode, e.Message);
                Environment.Exit(-1);
            }

        }


    }

    public string ip;
    private const short port = ConstValues.SERVER_PORT;
    public string Name;
    public string password;

    public bool Init()
    {
        islogin = false;
        recvBuf = new byte[1024];
        socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        socket.NoDelay = true;
        socket.SendBufferSize = 0;
        try
        {
            socket.Connect(ip, port);
        }
        catch (Exception ex)
        {
            UnityEngine.Debug.Log(ex.ToString());
            return false;
        }

        //접속요청 패킷보내기
        send_login_req();
        //접속 요청 패킷 받기
        return islogin = recv_login_ok();

    }

    public void ClientProcess()
    {


    }
    public void send_login_req()
    {
        var builder = new FlatBuffers.FlatBufferBuilder(1024);
        var id = builder.CreateString(Name);
        var pass = builder.CreateString(password);
        var data = fbs.C2S_LOGIN_REQ.CreateC2S_LOGIN_REQ(builder, id, pass);
        var root = fbs.Root.CreateRoot(builder, fbs.Packet.C2S_LOGIN_REQ, data.Value);
        builder.FinishSizePrefixed(root.Value);
        byte[] buf = builder.SizedByteArray();
        SendPacket(buf);
    }

    public void send_Enter_game()
    {
        var builder = new FlatBuffers.FlatBufferBuilder(1024);
        var data = fbs.C2S_Enter_GAME.CreateC2S_Enter_GAME(builder, 0);
        var root = fbs.Root.CreateRoot(builder, fbs.Packet.C2S_Enter_GAME, data.Value);
        builder.FinishSizePrefixed(root.Value);
        byte[] buf = builder.SizedByteArray();
        SendPacket(buf);
    }
    public void send_HeatBeatCheck()
    {
        var builder = new FlatBuffers.FlatBufferBuilder(1024);
        var data = fbs.Health_Check.CreateHealth_Check(builder,0);
        var root = fbs.Root.CreateRoot(builder, fbs.Packet.Health_Check, data.Value);
        builder.FinishSizePrefixed(root.Value);
        byte[] buf = builder.SizedByteArray();
        SendPacket(buf);
    }
    public void send_Chatt_message(string msg)
    {
        var builder = new FlatBuffers.FlatBufferBuilder(1024);

        var message = builder.CreateString(msg);
        var data = fbs.C2S_Chatt_Message.CreateC2S_Chatt_Message(builder, message);
        var root = fbs.Root.CreateRoot(builder, fbs.Packet.C2S_Chatt_Message, data.Value);
        builder.FinishSizePrefixed(root.Value);
        byte[] buf = builder.SizedByteArray();
        SendPacket(buf);
    }

    //public void send_Key_down_packet(fbs.Key key) //이거 왜있냐?
    //{
    //    var builder = new FlatBuffers.FlatBufferBuilder(1024);
    //    var data = fbs.C2S_Key_Down.CreateC2S_Key_Down(builder, key);
    //    var root = fbs.Root.CreateRoot(builder, fbs.Packet.C2S_Key_Down, data.Value);
    //    builder.FinishSizePrefixed(root.Value);
    //    byte[] buf = builder.SizedByteArray();
    //    SendPacket(buf);
    //}

    public void send_attack(fbs.Direction dir)
    {
        var builder = new FlatBuffers.FlatBufferBuilder(1024);
        var data = fbs.Attack_Packet.CreateAttack_Packet(builder,dir);
        var root = fbs.Root.CreateRoot(builder, fbs.Packet.Attack_Packet, data.Value);
        builder.FinishSizePrefixed(root.Value);
        byte[] buf = builder.SizedByteArray();
        SendPacket(buf);
    }
    public void send_Move_Req(fbs.Direction dir)
    {
        var builder = new FlatBuffers.FlatBufferBuilder(1024);
        var data = fbs.C2S_Move_REQ.CreateC2S_Move_REQ(builder, dir, 0);
        var root = fbs.Root.CreateRoot(builder, fbs.Packet.C2S_Move_REQ, data.Value);
        builder.FinishSizePrefixed(root.Value);
        byte[] buf = builder.SizedByteArray();
        SendPacket(buf);
    }
    bool recv_login_ok()
    {
        byte[] buf = new byte[1024];
        int recvSize = socket.Receive(buf, buf.Length, SocketFlags.None);

        var flattBuf = new FlatBuffers.ByteBuffer(buf, 4);

        var root = fbs.Root.GetRootAsRoot(flattBuf);
        var type = root.PacketType;
        if (recvSize > 0 && type == fbs.Packet.S2C_LOGIN_RESULT)
        {


            var packet = root.PacketAsS2C_LOGIN_RESULT();

            if (packet.Result == -1)
            {
                Debug.Log("암호 틀림");

                return false;
            }
            var xpos = packet.XPos;
            var ypos = packet.YPos;
            Debug.Log("로그인 성공~~" + xpos.ToString() + " , " + ypos.ToString());

            playerInitInfo = new PlayerInitInfo(packet.Id, packet.XPos, -packet.YPos, packet.MaxHp, packet.Dmg,packet.Lv,packet.CurExp);


            socket.Blocking = false;
            return true;

        }

        Debug.Log("로그인 실패~~");
        return false;

    }
    void SendPacket(byte[] buf)
    {
        socket.Send(buf, buf.Length, SocketFlags.None);
    }
}
