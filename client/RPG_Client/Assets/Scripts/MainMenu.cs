using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public NetworkMgr net;
    public Text IP;
    public Text ID;
    public Text password;
    public void OnClickLogin()
    {
        net.ip = IP.text;
        net.Name = ID.text;
        net.password = password.text;
        Debug.Log("로그인");
        Debug.Log(net.ip);
        Debug.Log(net.Name);
        Debug.Log(net.password);

        if (net.Init() == true)
        {
            Debug.Log("씬 바꿈");
            SceneManager.LoadScene("GameScene");//이거는 한 프레임 끝나고 적용됨
        }
        else
            Application.Quit();

    }
    public void OnClickQuit()
    {
        Debug.Log("종료");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
          Application.Quit();
#endif

    }
}
