using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAction : MonoBehaviour
{

    public int max_hp;
    public int cur_hp { get; set; }
    public int dmg;
    public float maxSpeed;

    public int lv { get; set; }
    public int cur_exp { get; set; }
    public GameObject Weaponprefeb;
    public Animator anim;
    Rigidbody2D rigid;
    //float h;
    //float v;
    public float moveKeyTime = 0f; //key딜레이를 위한시간
    public float attackSpeed = 1.0f;//1초에 한번 공격가능
    float moveTime = 0.0f;
    float attackTime = 0.0f;
    bool isMovePosible = true;
    public bool isAttackPosible = true;
    bool moveflag = false;
    bool isPlayer = false;
    enum Direction : int { Up = 0, Down, Left, Right };
    readonly float[] dirX = { 0f, 0f, -1f, 1f };
    readonly float[] dirY = { 1f, -1f, 0f, 0f };
    int dir;
    Vector3 StartPos;
    Vector3 EndPos;


    private Weapon myweapon;
    //UI
    public Text player_pos;
    void Awake()
    {
        //NetworkMgr.instance.player = this;
        //rigid = GetComponent<Rigidbody2D>();


        myweapon = Instantiate(Weaponprefeb, this.transform.position, Quaternion.identity).GetComponent<Weapon>();
        myweapon.transform.SetParent(this.transform);
  
    }
    void Update()
    {



        if (true == isPlayer)
        {
            InputMoveKey();
        }


        MoveUpdate();
        anim.SetInteger("Dir", dir);
        anim.SetBool("isMove", moveflag);

    }
    void FixedUpdate()
    {

    }
    public void SetMaxHp(int hp)
    {
        max_hp = hp;
        cur_hp = hp;
    }
    public void SetDmg(int _dmg)
    {
        dmg = _dmg;
    }

    public void SetHps(int max, int cur)
    {
        max_hp = max;
        cur_hp = cur;
    }

    public void Hitted(int hp)
    {
        cur_hp = hp;
    }
    public void InputMoveKey() //이동키 눌렀을때 처리
    {


        if (false == isMovePosible)
        {
            moveKeyTime += Time.deltaTime;
            if (moveKeyTime >= 1.0f)
            {
                //Debug.Log = Time.deltaTime;
                Debug.Log("1초");
                isMovePosible = true;
                moveKeyTime = 0.0f;
            }
        }
        else
        {

            if (Input.GetKey(KeyCode.DownArrow))
            {
                NetworkMgr.instance.send_Move_Req(fbs.Direction.Down);
                StartPos = this.transform.position;
                isMovePosible = false;
                anim.SetFloat("DirX", 0f);
                anim.SetFloat("DirY", -1f);
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                NetworkMgr.instance.send_Move_Req(fbs.Direction.Up);
                isMovePosible = false;
                StartPos = this.transform.position;
                anim.SetFloat("DirX", 0f);
                anim.SetFloat("DirY", 1f);

            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                NetworkMgr.instance.send_Move_Req(fbs.Direction.Left);
                StartPos = this.transform.position;
                isMovePosible = false;
                anim.SetFloat("DirX", -1f);
                anim.SetFloat("DirY", 0f);


            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                NetworkMgr.instance.send_Move_Req(fbs.Direction.Right);
                StartPos = this.transform.position;
                isMovePosible = false;

                anim.SetFloat("DirX", 1f);
                anim.SetFloat("DirY", 0f);
            }

        }


        if (false == isAttackPosible)
        {
            attackTime += Time.deltaTime;
            if (attackTime >= attackSpeed)
            {
                isAttackPosible = true;
                attackTime = 0.0f;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.Space))
            {
                isAttackPosible = false;

                anim.SetFloat("DirX", dirX[dir]);
                anim.SetFloat("DirY", dirY[dir]);
                anim.SetTrigger("AttackTrigger");
                myweapon.AttackWeapon(dirX[dir], dirY[dir]);
                
                NetworkMgr.instance.send_attack((fbs.Direction)dir);
            }
        }
    }


    public void moveStart(int x, int y)
    {
        var pos = this.transform.position;
        if (x < pos.x)//왼쪽으로 이동
        {
            dir = (int)Direction.Left;
            anim.SetFloat("DirX", -1f);
            anim.SetFloat("DirY", 0f);
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (x > pos.x)
        {
            dir = (int)Direction.Right;
            anim.SetFloat("DirX", 1f);
            anim.SetFloat("DirY", 0f);
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (y > pos.y)
        {
            dir = (int)Direction.Up;
            anim.SetFloat("DirX", 0f);
            anim.SetFloat("DirY", 1f);
        }
        else if (y < pos.y)
        {
            dir = (int)Direction.Down;
            anim.SetFloat("DirX", 0f);
            anim.SetFloat("DirY", -1f);
        }

        moveflag = true;
        moveTime = 0.0f;
        StartPos = pos;
        EndPos = new Vector3(x, y, 1f);
    }
    void MoveUpdate()
    {
        if (true == moveflag)
        {
            moveTime += Time.deltaTime;
            if (moveTime > 1.0f)
            {
                moveTime = 1.0f;
                moveflag = false;


            }
            this.transform.position = ((1.0f - moveTime) * StartPos) + (moveTime * EndPos);
        }
        else
        {
            float x = Mathf.Round(this.transform.position.x);
            float y = Mathf.Round(this.transform.position.y);
            this.transform.position = new Vector3(x, y, 0.0f);
        }
    }

    public bool IsPlayer() { return isPlayer; }
    public void SetIsPlayerTrue() { isPlayer = true; }
   
}
