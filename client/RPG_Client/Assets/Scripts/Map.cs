using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject MapPrefab;
    GameObject[] Maps;
    const int width = 20;
  GameObject player;
    int x, y;
    int[] dx = { -width, 0, width, -width, 0, width, -width, 0, width };
    int[] dy = { width, width, width, 0, 0, 0, -width, -width, -width };
    void Awake()
    {



    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
            return;
        int X = (int)player.transform.position.x;
        int Y = (int)player.transform.position.y;
        X = (X / width) * width;
        Y = (Y / width) * width;
        if (x == X && y == Y)
            return;
        x = X;
        y = Y;


        for (int i = 0; i < 9; ++i)
        {
            Maps[i].transform.position = new Vector3(x + dx[i], y + dy[i], 10);
        }


    }

    public void Init(GameObject  p)
    {
        player=p;
        int X = (int)player.transform.position.x;
        int Y = (int)player.transform.position.y;
        X = (X / width) * width;
        Y = (Y / width) * width;
        x = X;
        y = Y;
        Maps = new GameObject[9];
        for (int i = 0; i < 9; ++i)
        {
            Maps[i] = Instantiate(MapPrefab);
            Maps[i].transform.SetParent(this.transform);
            Maps[i].transform.position = new Vector3(x + dx[i], y + dy[i], 10);


        }
    }
}
