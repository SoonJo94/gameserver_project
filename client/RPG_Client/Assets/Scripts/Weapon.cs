using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator anim;
    public float aniTime = 0.8f;

    void Start()
    {

        anim = GetComponent<Animator>();
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= aniTime)
            gameObject.SetActive(false);
    }


    public void AttackWeapon(float dirX, float dirY)
    {
        if (dirY == 1f)
        {
            gameObject.layer = 0;
        }
        else
            gameObject.layer = 2;
        gameObject.SetActive(true);
        anim.SetFloat("DirX", dirX);
        anim.SetFloat("DirY", dirY);
        anim.SetTrigger("AttackTrigger");
    
    }

}
