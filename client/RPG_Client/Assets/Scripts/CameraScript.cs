using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraScript : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject player;
    public static CameraScript instance = null;
    SpriteRenderer[] mapImage;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else if (instance != this)
            Destroy(this.gameObject);


    }
    public bool inGame = false;
    // Update is called once per frame
    void Update()
    {
        if (inGame == true) { 
            this.transform.position = player.transform.position;
         
            transform.Translate(0, 0, -10);
        }
    }
}
