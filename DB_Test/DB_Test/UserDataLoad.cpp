// SQLBindCol_ref.cpp  
// compile with: odbc32.lib  
#include <windows.h>  
#include <iostream>  
#include <string>
#include <vector>
#define UNICODE  
#include <sqlext.h>  
#include <thread>
#include <mutex>
#define NAME_LEN 50  

constexpr int TESTCNT = 1'000'000;
using namespace std;
using namespace std::chrono;
void show_error() {
	printf("error\n");
}
/************************************************************************
/* HandleDiagnosticRecord : display error/warning information
/*
/* Parameters:
/*      hHandle     ODBC handle
/*      hType       Type of handle (HANDLE_STMT, HANDLE_ENV, HANDLE_DBC)
/*      RetCode     Return code of failing command
/************************************************************************/

void HandleDiagnosticRecord(SQLHANDLE      hHandle,
	SQLSMALLINT    hType,
	RETCODE        RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];


	if (RetCode == SQL_INVALID_HANDLE)
	{
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}

	while (SQLGetDiagRec(hType,
		hHandle,
		++iRec,
		wszState,
		&iError,
		wszMessage,
		(SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)),
		(SQLSMALLINT*)NULL) == SQL_SUCCESS)
	{
		// Hide data truncated..
		if (wcsncmp(wszState, L"01004", 5))
		{
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
}


void TestQQ(int id);
mutex g_lock;
int main()
{
	_wsetlocale(LC_ALL, L"Korean");
	TestQQ(0);
	//std::vector<thread> v;

	//for (int i = 0; i < 4; ++i)
	//	v.emplace_back(TestQQ, i);

	//for (auto& th : v)
	//	th.join();

}

void TestQQ(int id)
{

	SQLHENV henv;
	SQLHDBC hdbc;
	//SQLHSTMT hstmt = 0;
	SQLRETURN retcode;

	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"GameServerDB", SQL_NTS, (SQLWCHAR*)NULL, 0, NULL, 0);
				//retcode = SQLDriverConnect(hdbc, NULL, (SQLWCHAR*)L"GameServerDB", SQL_NTS, NULL, 0, NULL, SQL_DRIVER_NOPROMPT);

				// Allocate statement handle  
				if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
					cout << "ODBC connect OK\n";


					//vector<thread> v;

						//lock_guard<mutex> lg(g_lock);

					SQLHSTMT hstmt = 0;
					SQLRETURN retcode = 0;


					retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);


					wstring op;
					wstring idx = to_wstring(id);
					op = L"EXEC ";

					op += L"LoadUserInfo "; //SelectUserInfo
					op += L"HSJa" + idx;

					SQLWCHAR szUser_name[NAME_LEN] = { 0, };
					SQLINTEGER  dUser_idx = 0;
					SQLLEN cbName = 0, cbIdx = 0;

					retcode = SQLExecDirect(hstmt, (SQLWCHAR*)op.c_str(), SQL_NTS);//명령어 넣는부분

				
					if (retcode == SQL_SUCCESS) {

						retcode = SQLFetch(hstmt);
						if (retcode == SQL_SUCCESS) {
							SQLINTEGER X_pos{ 0 }, Y_pos{ 0 };
							SQLLEN intLen{ SQL_INTEGER };
							retcode = SQLGetData(hstmt, 1, SQL_INTEGER, &X_pos,10, &intLen);
							retcode = SQLGetData(hstmt, 2, SQL_INTEGER, &Y_pos, 10, &intLen);
							cout << X_pos << ' ' << Y_pos << '\n';
						}

					}
					else {

						HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
					}
					SQLFreeHandle(SQL_HANDLE_STMT, hstmt);



					SQLDisconnect(hdbc);
				}
				else
					HandleDiagnosticRecord(hdbc, SQL_HANDLE_DBC, retcode);
				SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
			}
		}
		SQLFreeHandle(SQL_HANDLE_ENV, henv);
	}
}
