# GAMESERVER_PROJECT

프로젝트 설명 

TopDown 형식의 MMORPG입니다.
멀티플랫폼에서 서로 연동되는 MMORPG를 개발하는 프로젝트입니다.


클라이언트는 유니티 입니다.

서버는 C++이며 현재 저만의 게임서버라이브러리를 만들려고 진행중입니다. 



*주요 파일 및 폴더 소개*

client 폴더
: 유니티 프로젝트가 들어있는 폴더입니다.

Query_Server 폴더
:DB와 게임서버를 중간에서 연결해주는 쿼리 서버 프로젝트가 있는 폴더입니다.

MyGameServer 폴더
: 주요 게임서버 프로젝트와 더미테스트 프로젝트가 있는 폴더입니다.

MyGameServer\SJ_SEVER_ENGINEr 폴더
:게임서버라이브러리를 만드는 폴더입니다.

MyGameServer\SeverEngine
:게임서버라이브러리에서 만든 결과물이 export되는 폴더입니다.

MyGameServer\ChattingServer
:게임서버의 로직이 수행되는 프로젝트의 폴더입니다. 해당 프로젝트는 게임서버라이브러내용을 import해서 사용하고 있습니다.

MyGameServer\TestClient
:게임서버의 성능(동접)을 측정하기 위한 프로젝트가 있는 폴더입니다.
